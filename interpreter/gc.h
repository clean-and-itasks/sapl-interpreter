#ifndef __GC_H
#define __GC_H

#include "mem.h"

void gc();

extern int gc_enabled;

#endif // __GC_H