#ifndef __PARSE_H
#define __PARSE_H

#include "code.h"

#define MAX_IDENTIFIER_LENGTH	256

int parse(char** ptr, int length);

Code* parseTerm(char **ptr);

#endif // __PARSE_H