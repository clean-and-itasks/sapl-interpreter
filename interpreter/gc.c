#include <string.h>
#include <stdio.h>

#include "debug.h"
#include "gc.h"
#include "thunk.h"
#include "desc.h"

#define follow_thunk(thunk) if (thunk->desc == (Desc*) __FORWARD_PTR__) thunk = thunk->_forward_ptr;

// http://stackoverflow.com/questions/17095324/fastest-way-to-determine-if-an-integer-is-between-two-integers-inclusive-with/17095534#17095534
//#define inheap(addr) ((unsigned)((int)addr-(int)heap_base_curr) < HEAP_SIZE)  
#define inheap(addr) ((char*)addr >= heap_base_curr && (char*)addr < (heap_base_curr + HEAP_SIZE))

int gc_enabled = 1;

void gc()
{
    if(!gc_enabled) return;
//    gc_enabled = 0;

#ifdef DEBUG  
    gc_cnt++;
#endif
    
#ifdef DEBUG_GC
    printf("GC %i START\n", gc_cnt);
    printf("ALLOC BEFORE: %i\n", (int) (heap_curr - heap_base_curr));
#endif
    
    char* allocptr = heap_base_swap;
    char* scanptr = heap_base_swap;
        
    for(int i=0; i<stack_top_a; i++)
    {
        Thunk* ptr = stack_a[i];
        
        // do not copy forward pointers
        // also do it outside, because the final thunk may be out of the heap            
        //follow_thunk(ptr);
                       
        if(!inheap(ptr)) continue;
        
        while (ptr->desc == (Desc*) __FORWARD_PTR__)
        {
            ptr = ptr->_forward_ptr;
            stack_a[i] = ptr;
        }
        
        if(inheap(ptr))
        {
            if(ptr->desc == NULL) // already copied
            {
                stack_a[i] = ptr->_forward_ptr;
            }
            else
            {
                int size;
                
                if(ptr->desc == (Desc*) __ARRAY__)
                {
                    size = sizeof (Desc) + sizeof (Array) + (ptr->_array.bytes_per_elem * ptr->_array.length);
                }
                else
                {
                    size = ptr->desc->thunk_size;
                }
                
                memcpy(allocptr, ptr, size);
                ptr->desc = NULL;
                ptr->_forward_ptr = (Thunk*) allocptr;
                stack_a[i] = (Thunk*) allocptr;
                allocptr += size;
            }
        }
    }

#ifdef DEBUG_GC
    printf("ALLOC AFTER FIRST STAGE: %i\n", allocptr - heap_base_swap);
#endif

    int arity;
    Thunk** args = NULL;
    
    while(scanptr < allocptr)
    {
        Thunk* ptr = (Thunk*) scanptr;
        
        if(ptr->desc == (Desc*) __ARRAY__)
        {
            scanptr += sizeof (Desc) + sizeof (Array) + (ptr->_array.bytes_per_elem * ptr->_array.length);

            if(ptr->_array.is_boxed)
            {
                arity = 0;
            }
            else
            {
                arity = ptr->_array.length;
                args = ptr->_array._elems;
            }
        }
        else
        {
            scanptr += ptr->desc->thunk_size;
            arity = ptr->desc->arity;
            args = ptr->_args;
        }
        
        for(int i=0; i<arity; i++)
        {
            Thunk* arg = (Thunk*) args[i];        

            // do not copy forward pointers
            // also do it outside, because the final thunk may be out of the heap
            // follow_thunk(arg);

            if(!inheap(arg)) continue;

            while (arg->desc == (Desc*) __FORWARD_PTR__)
            {
                arg = arg->_forward_ptr;
                ptr->_args[i] = arg;
            }
                
            if(inheap(arg) /*&& ptr->desc != NULL*/)
            {
                if(arg->desc == NULL)
                {
                    ptr->_args[i] = arg->_forward_ptr;
                }
                else
                {
                    int size;

                    if(arg->desc == (Desc*) __ARRAY__)
                    {
                        size = sizeof (Desc) + sizeof (Array) + (arg->_array.bytes_per_elem * arg->_array.length);
                    }
                    else
                    {
                        size = arg->desc->thunk_size;
                    }

                    memcpy(allocptr, arg, size);
                    arg->desc = NULL;
                    arg->_forward_ptr = (Thunk*) allocptr;
                    ptr->_args[i] = (Thunk*) allocptr;
                    allocptr += size;                    
                }
            }            
        }
    }

#ifdef DEBUG_GC    
    printf("ALLOC AFTER SECOND STAGE: %i\n", allocptr - heap_base_swap);    
#endif
    
    heap_curr = allocptr;
    char *tmp = heap_base_curr;
    heap_base_curr = heap_base_swap;
    heap_base_swap = tmp;
    gc_trigger = heap_base_curr + HEAP_SIZE - 1024;

#ifdef DEBUG_GC
    printf("GC %i END\n", gc_cnt);
#endif

}
