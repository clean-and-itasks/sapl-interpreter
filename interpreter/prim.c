#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "debug.h"
#include "prim.h"
#include "desc.h"
#include "thunk.h"
#include "mem.h"

#define arg(idx) stack_a[stack_top_a - idx]

void __addI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) + readI(arg(1));
}

void __subI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) - readI(arg(1));
}

void __multI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) * readI(arg(1));
}

void __divI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) / readI(arg(1));
}

void __bitxorI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) ^ readI(arg(1));
}

void __bitandI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) & readI(arg(1));
}

void __bitorI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) | readI(arg(1));
}

void __bitnotI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = ~readI(arg(1));
}

void __shiftleftI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) << readI(arg(1));
}

void __shiftrightI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) >> readI(arg(1));
}

void __addR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = readR(arg(2)) + readR(arg(1));
}

void __subR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = readR(arg(2)) - readR(arg(1));
}

void __multR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = readR(arg(2)) * readR(arg(1));
}

void __divR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = readR(arg(2)) / readR(arg(1));
}

void __negR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = -readR(arg(1));
}

void __cos(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = cos(readR(arg(1)));
}

void __sin(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = sin(readR(arg(1)));
}

void __tan(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = tan(readR(arg(1)));
}

void __acos(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = acos(readR(arg(1)));
}

void __atan(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = atan(readR(arg(1)));
}

void __absR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    double val = readR(arg(1));
    target->_real = val < 0 ? -val : val;
}

void __gtI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readI(arg(2)) > readI(arg(1));
}

void __geI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readI(arg(2)) >= readI(arg(1));
}

void __ltI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readI(arg(2)) < readI(arg(1));
}

void __geC(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readC(arg(2)) >= readC(arg(1));
}

void __ltC(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readC(arg(2)) < readC(arg(1));
}

void __ltR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readR(arg(2)) < readR(arg(1));
}

void __eqI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readI(arg(2)) == readI(arg(1));
}

void __eqR(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readR(arg(2)) == readR(arg(1));
}

void __neqI(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readI(arg(2)) != readI(arg(1));
}

void __eqB(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readB(arg(2)) == readB(arg(1));
}

void __eqC(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readC(arg(2)) == readC(arg(1));
}

void __not(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = !readB(arg(1));
}

void __and(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readB(arg(2)) && readB(arg(1));
}

void __or(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __BOOL__;
    target->_int = readB(arg(2)) || readB(arg(1));
}

void __mod(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readI(arg(2)) % readI(arg(1));
}

void __C2I(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    target->_int = readC(arg(1));
}

void __I2C(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __CHAR__;
    target->_int = (char) readI(arg(1));
}

void __I2R(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = readI(arg(1));
}

void __R2R(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = readR(arg(1));
}

void __R2I(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    double v = readR(arg(1));
    target->_int = v >= 0 ? (long)(v+0.5) : (long)(v-0.5);
}

char buff[30];

void __S2R(int dst_idx) {
    Thunk* str = arg(1);
    
    int length;
    char* chars;
    
    if(str->desc == (Desc*) __STRING_PTR__)
    {
        chars = str->_string_ptr->chars;
        length = str->_string_ptr->length;
    }
    else
    {
        chars = str->_array._chars;
        length = str->_array.length;
    }

    if(length > 29) length = 29;
    memcpy(buff, chars, length);
    buff[length] = '\0';
    
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __REAL__;
    target->_real = strtod(buff, NULL);
}

void __S2I(int dst_idx) {
    Thunk* str = arg(1);
    
    int length;
    char* chars;
    
    if(str->desc == (Desc*) __STRING_PTR__)
    {
        chars = str->_string_ptr->chars;
        length = str->_string_ptr->length;
    }
    else
    {
        chars = str->_array._chars;
        length = str->_array.length;
    }

    if(length > 19) length = 19;
    memcpy(buff, chars, length);
    buff[length] = '\0';
    
    Thunk* target = get_dst(dst_idx);
    target->desc = (Desc*) __INT__;
    // use atol instead of strtol, the former overflows (Clean semantics), the latter returns MAX 
    target->_int = atol(buff);
}

void __string_size(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arg = arg(1);
    
    int length;
    
    if(arg->desc == (Desc*) __STRING_PTR__)
    {
        length = arg->_string_ptr->length;
    }
    else
    {
        length = arg->_array.length;
    }
    
    target->desc = (Desc*) __INT__;
    target->_int = length;
}

void __array_size(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(1);
    
    target->desc = (Desc*) __INT__;
    target->_int = arr->_array.length;
}

void __string_select(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* str = arg(2);
    int pos = readI(arg(1));
    
    char* chars;
    
    if(str->desc == (Desc*) __STRING_PTR__)
    {
        chars = str->_string_ptr->chars;
    }
    else
    {
        chars = str->_array._chars;
    }
    
    target->desc = (Desc*) __CHAR__;
    target->_int = (char) chars[pos];
}


void __array_select(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(2);
    int pos = readI(arg(1));

    Thunk* elem = arr->_array._elems[pos];
    elem->desc->eval();
    
    if(target != NULL)
    {
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = elem;
    }
    
    set_return(dst_idx, elem);
}

void __array_select_b_b(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(2);
    int pos = readI(arg(1));

    target->desc = (Desc*) __BOOL__;
    target->_int = (int) arr->_array._bools[pos];
}

void __array_select_b_i(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(2);
    int pos = readI(arg(1));

    target->desc = (Desc*) __INT__;
    target->_int = arr->_array._ints[pos];
}

void __array_select_b_r(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(2);
    int pos = readI(arg(1));

    target->desc = (Desc*) __REAL__;
    target->_real = arr->_array._reals[pos];
}

Thunk* array_create(Thunk* target, int elementsize, int len, ArrayElementType type)
{
    int newsize = sizeof (Desc) + sizeof (Array) + len * elementsize;
    
    if(target == NULL)
    {
        target = (Thunk*) alloc_heap(newsize);
    }
    else if(target->desc->thunk_size < newsize)
    {
        Thunk* tmp = (Thunk*) alloc_heap(newsize);
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = tmp;
        target = tmp;
    }

    target->desc = (Desc*) __ARRAY__;
    target->_array.type = type;
    target->_array.is_boxed = type != AET_OTHER;
    target->_array.bytes_per_elem = elementsize;
    target->_array.length = len;
    
    return target;
}

Thunk* string_create(Thunk* target, int len)
{
    return array_create(target, 1, len, AET_CHAR);
}

void __string_create1(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(1));
    set_return(dst_idx, string_create(target, len));   
}

void __array_create1(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(1));
    set_return(dst_idx, array_create(target, sizeof(void*), len, AET_OTHER));   
}

void __array_create1_b_i(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(1));
    set_return(dst_idx, array_create(target, sizeof(int), len, AET_INT));   
}

void __array_create1_b_b(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(1));
    set_return(dst_idx, array_create(target, sizeof(char), len, AET_BOOL));   
}

void __array_create1_b_r(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(1));
    set_return(dst_idx, array_create(target, sizeof(double), len, AET_REAL));   
}

void __string_create2(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(2));
    char ch = readC(arg(1));
    
    target = string_create(target, len);
    for(int i=0; i<len; i++)
    {
        target->_array._chars[i] = (char) ch;    
    }
    
    set_return(dst_idx, target);   
}

void __array_create2(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(2));
    Thunk* def = arg(1);
    
    target = array_create(target, sizeof(Thunk*), len, AET_OTHER);
    for(int i=0; i<len; i++)
    {
        target->_array._elems[i] = def;    
    }
    
    set_return(dst_idx, target);   
}

void __array_create2_b_i(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(2));
    Thunk* def = arg(1);
    
    target = array_create(target, sizeof(int), len, AET_INT);
    for(int i=0; i<len; i++)
    {
        target->_array._ints[i] = def->_int;    
    }
    
    set_return(dst_idx, target);   
}

void __array_create2_b_b(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(2));
    int def = readB(arg(1));
    
    target = array_create(target, sizeof(char), len, AET_BOOL);
    for(int i=0; i<len; i++)
    {
        target->_array._bools[i] = def;    
    }
    
    set_return(dst_idx, target);   
}

void __array_create2_b_r(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    int len = readI(arg(2));
    double def = readR(arg(1));
    
    target = array_create(target, sizeof(double), len, AET_REAL);
    for(int i=0; i<len; i++)
    {
        target->_array._reals[i] = def;    
    }
    
    set_return(dst_idx, target);   
}

void __string_update_copy(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* str = arg(3);
    int idx = readI(arg(2));
    char ch = readC(arg(1));
        
    int length;
    char* chars;
    
    if(str->desc == (Desc*) __STRING_PTR__)
    {
        chars = str->_string_ptr->chars;
        length = str->_string_ptr->length;
    }
    else
    {
        chars = str->_array._chars;
        length = str->_array.length;
    }
    
    target = string_create(target, length);
    memcpy(target->_array._chars, chars, length);

    target->_array._chars[idx] = (char) ch;
    
    set_return(dst_idx, target);   
}

void __string_update(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(3);
    int idx = readI(arg(2));
    char elem = readC(arg(1));
            
    arr->_array._chars[idx] = (char) elem;

    if(target != NULL)
    {
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = arr;
    }
    
    set_return(dst_idx, arr);   
}

void __array_update(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(3);
    int idx = readI(arg(2));
    Thunk* elem = arg(1);
            
    arr->_array._elems[idx] = elem;
    
    if(target != NULL)
    {
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = arr;
    }

    set_return(dst_idx, arr);   
}

void __array_update_b_i(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(3);
    int idx = readI(arg(2));
    int elem = readI(arg(1));
            
    arr->_array._ints[idx] = elem;

    if(target != NULL)
    {
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = arr;
    }
    
    set_return(dst_idx, arr);   
}

void __array_update_b_b(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(3);
    int idx = readI(arg(2));
    int elem = readB(arg(1));
            
    arr->_array._bools[idx] = (unsigned char) elem;

    if(target != NULL)
    {
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = arr;
    }
    
    set_return(dst_idx, arr);   
}

void __array_update_b_r(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* arr = arg(3);
    int idx = readI(arg(2));
    double elem = readR(arg(1));
            
    arr->_array._reals[idx] = elem;

    if(target != NULL)
    {
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = arr;
    }
    
    set_return(dst_idx, arr);   
}

void __string_slice(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* str = arg(3);
    int idx1 = readI(arg(2));
    int idx2 = readI(arg(1));
        
    char* chars;
    int full_length;
    
    if(str->desc == (Desc*) __STRING_PTR__)
    {
        chars = str->_string_ptr->chars;
        full_length = str->_string_ptr->length;
    }
    else
    {
        chars = str->_array._chars;
        full_length = str->_array.length;
    }

    int length = idx2 - idx1 + 1;
    if(idx1 + length > full_length) length = full_length - idx1;
    
    target = string_create(target, length);
    memcpy(target->_array._chars, chars + idx1, length);
    
    set_return(dst_idx, target);       
}

void __string_append(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* str1 = arg(2);
    Thunk* str2 = arg(1);
        
    int length1;
    char* chars1;
    
    if(str1->desc == (Desc*) __STRING_PTR__)
    {
        chars1 = str1->_string_ptr->chars;
        length1 = str1->_string_ptr->length;
    }
    else
    {
        chars1 = str1->_array._chars;
        length1 = str1->_array.length;
    }

    int length2;
    char* chars2;
    
    if(str2->desc == (Desc*) __STRING_PTR__)
    {
        chars2 = str2->_string_ptr->chars;
        length2 = str2->_string_ptr->length;
    }
    else
    {
        chars2 = str2->_array._chars;
        length2 = str2->_array.length;
    }
            
    target = string_create(target, length1 + length2);
    
    memcpy(target->_array._chars, chars1, length1);
    memcpy(target->_array._chars + length1, chars2, length2);
    
    set_return(dst_idx, target);       
}

void __eqS(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* str1 = arg(2);
    Thunk* str2 = arg(1);
        
    int length1;
    char* chars1;
    
    if(str1->desc == (Desc*) __STRING_PTR__)
    {
        chars1 = str1->_string_ptr->chars;
        length1 = str1->_string_ptr->length;
    }
    else
    {
        chars1 = str1->_array._chars;
        length1 = str1->_array.length;
    }

    int length2;
    char* chars2;
    
    if(str2->desc == (Desc*) __STRING_PTR__)
    {
        chars2 = str2->_string_ptr->chars;
        length2 = str2->_string_ptr->length;
    }
    else
    {
        chars2 = str2->_array._chars;
        length2 = str2->_array.length;
    }
          
    int eq = length1 == length2;
    
    int i = 0;
    while(i<length1 && eq)
    {
        eq = chars1[i] == chars2[i];
        i++;
    }

    target->desc = (Desc*) __BOOL__;
    target->_int = eq;    
}

void __ltS(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* str1 = arg(2);
    Thunk* str2 = arg(1);
        
    int length1;
    char* chars1;
    
    if(str1->desc == (Desc*) __STRING_PTR__)
    {
        chars1 = str1->_string_ptr->chars;
        length1 = str1->_string_ptr->length;
    }
    else
    {
        chars1 = str1->_array._chars;
        length1 = str1->_array.length;
    }

    int length2;
    char* chars2;
    
    if(str2->desc == (Desc*) __STRING_PTR__)
    {
        chars2 = str2->_string_ptr->chars;
        length2 = str2->_string_ptr->length;
    }
    else
    {
        chars2 = str2->_array._chars;
        length2 = str2->_array.length;
    }
     
    int result = length1 == 0;
    
    if(length1 > 0 && length2 > 0)
    {
        result = length1 < length2;
        
        int i = 0;
        while(i<length1 && i<length2)
        {
            if(chars1[i] != chars2[i])
            {
                result = chars1[i] < chars2[i];
                break;
            }
            
            i++;
        }
    }
    
    target->desc = (Desc*) __BOOL__;
    target->_int = result;    
}

void __C2S(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    char ch = readC(arg(1));

    target = string_create(target, 1);
    target->_array._chars[0] = (char) ch;    
    set_return(dst_idx, target);
}

void __I2S(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    int i = readI(arg(1));

    itoa(i, buff, 10);
    int len = strlen(buff);
    
    target = string_create(target, len);
    memcpy(&target->_array._chars, buff, len);
    set_return(dst_idx, target);
}

void __R2S(int dst_idx) {
    Thunk* target = get_dst(dst_idx);
    double r = readR(arg(1));

    int len = snprintf(buff,30,"%g",r);
    
    target = string_create(target, len);
    memcpy(target->_array._chars, buff, len);
    set_return(dst_idx, target);
}

void __trace(int dst_idx)
{
    Thunk* str = arg(1);

    int length;
    char* chars;
    
    if(str->desc == (Desc*) __STRING_PTR__)
    {
        chars = str->_string_ptr->chars;
        length = str->_string_ptr->length;
    }
    else
    {
        chars = str->_array._chars;
        length = str->_array.length;
    }

    for(int i=0; i<length; i++)
    {
        printf("%c", chars[i]);
    }
}

void __abort(int dst_idx)
{
    __trace(dst_idx);
    exit(-1);
}

void __select(int dst_idx)
{
    Thunk* target = get_dst(dst_idx);
    Thunk* cons = arg(2);
    int idx = readI(arg(1));

    push_a(cons->_args[idx]);
    cons->_args[idx]->desc->eval();
    
    Thunk* ret = pop_a();
    
    if(target != NULL)
    {
        target->desc = (Desc*) __FORWARD_PTR__;
        target->_forward_ptr = ret;
    }
    
    set_return(dst_idx, ret);
}

PrimEntry* add_prim(int arity, int boxingMap, int unboxableReturn,  char* name, void (*exec)(int)) {
    int nameLength = strlen(name);

    // before the PrimEntry there are "arity" number of SliceEntries
    SliceEntry* entry_base = (SliceEntry*) alloc_desc(sizeof (SliceEntry) * arity + sizeof (PrimEntry) + nameLength + 1);

    PrimEntry* entry = (PrimEntry*) (entry_base + arity);
    entry->base.type = FT_PRIM;
    entry->base.arity = arity;
    entry->base.thunk_size = thunk_size_f(arity);    
    entry->base.hnf = false;
    entry->base.unboxable_return = unboxableReturn;
    entry->boxingMap = boxingMap;
    entry->exec = exec;

    // TODO: should it be copied at all?
    memcpy(entry->name, name, nameLength);
    entry->name[nameLength] = '\0';

    // generate slices. avoid function call if arity is zero
    if (arity > 0) gen_slices(entry_base, (Desc*) entry, arity);

    add_desc(entry->name, (Desc*) entry);
    return entry;
}

void init_prim() {
    add_prim(2, 0b011, 1, "addI", &__addI);
    add_prim(2, 0b011, 1, "subI", &__subI);
    add_prim(2, 0b011, 1, "multI", &__multI);
    add_prim(2, 0b011, 1, "divI", &__divI);
    add_prim(2, 0b011, 1, "gtI", &__gtI);
    add_prim(2, 0b011, 1, "geI", &__geI);
    add_prim(2, 0b011, 1, "ltI", &__ltI);
    add_prim(2, 0b011, 1, "eqI", &__eqI);
    add_prim(2, 0b011, 1, "neqI", &__neqI);
    
    add_prim(2, 0b011, 1, "bitxor", &__bitxorI);
    add_prim(2, 0b011, 1, "bitand", &__bitandI);
    add_prim(2, 0b011, 1, "bitor", &__bitorI);
    add_prim(1, 0b001, 1, "bitnot", &__bitnotI);
    add_prim(2, 0b011, 1, "shiftleft", &__shiftleftI);
    add_prim(2, 0b011, 1, "shiftright", &__shiftrightI);
    
    add_prim(2, 0b011, 1, "geC", &__geC);    
    add_prim(2, 0b011, 1, "ltC", &__ltC);
    add_prim(2, 0b011, 1, "eqC", &__eqC);

    add_prim(2, 0b011, 1, "addR", &__addR);
    add_prim(2, 0b011, 1, "subR", &__subR);
    add_prim(2, 0b011, 1, "multR", &__multR);
    add_prim(2, 0b011, 1, "divR", &__divR);
    add_prim(1, 0b001, 1, "cos", &__cos);
    add_prim(1, 0b001, 1, "sin", &__sin);
    add_prim(1, 0b001, 1, "tan", &__tan);
    add_prim(1, 0b001, 1, "acos", &__acos);
    add_prim(1, 0b001, 1, "atan", &__atan);
    add_prim(1, 0b001, 1, "absR", &__absR);
    add_prim(1, 0b001, 1, "negR", &__negR);
    add_prim(2, 0b011, 1, "ltR", &__ltR);
    add_prim(2, 0b011, 1, "eqR", &__eqR);
    
    add_prim(2, 0b011, 1, "eqB", &__eqB);
    add_prim(1, 0b001, 1, "not", &__not);
    add_prim(2, 0b011, 1, "and", &__and); 
    add_prim(2, 0b011, 1, "or", &__or); 
    add_prim(2, 0b011, 1, "mod", &__mod);
    
    add_prim(1, 0b001, 1, "C2I", &__C2I);
    add_prim(1, 0b001, 1, "R2I", &__R2I);
    add_prim(1, 0b000, 1, "S2I", &__S2I);
    add_prim(1, 0b001, 1, "I2C", &__I2C);
    add_prim(1, 0b001, 1, "I2R", &__I2R);
    add_prim(1, 0b001, 1, "R2R", &__R2R);
    add_prim(1, 0b000, 1, "S2R", &__S2R);
            
    add_prim(1, 0b001, 0, "array_create1", &__array_create1);
    add_prim(1, 0b001, 0, "array_create1_lazy", &__array_create1);
    add_prim(1, 0b001, 0, "array_create1_B_I", &__array_create1_b_i);
    add_prim(1, 0b001, 0, "array_create1_B_B", &__array_create1_b_b);
    add_prim(1, 0b001, 0, "array_create1_B_R", &__array_create1_b_r);

    add_prim(2, 0b001, 0, "array_create2", &__array_create2);
    add_prim(2, 0b001, 0, "array_create2_lazy", &__array_create2); // TODO: should not be strict
    add_prim(2, 0b011, 0, "array_create2_B_I", &__array_create2_b_i);
    add_prim(2, 0b011, 0, "array_create2_B_B", &__array_create2_b_b);
    add_prim(2, 0b011, 0, "array_create2_B_R", &__array_create2_b_r);
        
    add_prim(3, 0b010, 0, "array_update", &__array_update);
    add_prim(3, 0b010, 0, "array_update_lazy", &__array_update); // TODO: should not be strict
    add_prim(3, 0b110, 0, "array_update_B_I", &__array_update_b_i);
    add_prim(3, 0b110, 0, "array_update_B_B", &__array_update_b_b);
    add_prim(3, 0b110, 0, "array_update_B_R", &__array_update_b_r);
    
    add_prim(1, 0b000, 1, "array_size", &__array_size);
    add_prim(1, 0b000, 1, "array_size_lazy", &__array_size);
    add_prim(1, 0b000, 1, "array_size_B_I", &__array_size);
    add_prim(1, 0b000, 1, "array_size_B_B", &__array_size);
    add_prim(1, 0b000, 1, "array_size_B_R", &__array_size);
    
    add_prim(2, 0b010, 0, "array_select", &__array_select);
    add_prim(2, 0b010, 0, "array_select_lazy", &__array_select);
    add_prim(2, 0b010, 1, "array_select_B_I", &__array_select_b_i);
    add_prim(2, 0b010, 1, "array_select_B_B", &__array_select_b_b);
    add_prim(2, 0b010, 1, "array_select_B_R", &__array_select_b_r);
    
    add_prim(1, 0b000, 1, "string_size", &__string_size);
    add_prim(2, 0b010, 1, "string_select", &__string_select);
    add_prim(1, 0b001, 0, "string_create1", &__string_create1);
    add_prim(2, 0b011, 0, "string_create2", &__string_create2);    
    add_prim(3, 0b110, 0, "string_update", &__string_update);
    add_prim(3, 0b110, 0, "string_update_copy", &__string_update_copy);
    add_prim(3, 0b110, 0, "string_slice", &__string_slice);
    add_prim(2, 0b000, 0, "string_append", &__string_append);
    add_prim(2, 0b000, 1, "eqS", &__eqS);
    add_prim(2, 0b000, 1, "ltS", &__ltS);
    add_prim(1, 0b001, 0, "C2S", &__C2S);
    add_prim(1, 0b001, 0, "I2S", &__I2S);
    add_prim(1, 0b001, 0, "R2S", &__R2S);
    
    add_prim(1, 0b000, 0, "_trace", &__trace);
    add_prim(1, 0b000, 0, "abort", &__abort);
    selectDesc = add_prim(2, 0b001, 0, "_select", &__select);
}

// For lazy select
PrimEntry* selectDesc;

