#ifndef __DESC_H
#define __DESC_H

#include "desc_base.h"
#include "thunk.h"
#include "code.h"

struct FunEntry {
    struct Desc base;
    int strictness;
    int boxing;

    union {
        char* parseCont;
        struct Code* body;
    };
    char name[];
};

// an array of these is just before an ADTEntry/FunEntry (as many as arity)

struct SliceEntry {
    struct Desc base;
    Desc* forward_ptr; // FunEntry or ADTEntry
};

struct ADTEntry {
    struct Desc base;
    int strictness;
    int boxing;
        
    unsigned int nrConses;  // number of constructors in the type
    unsigned int idx;       // constructor index
    
    char name[];
};

struct CAFEntry {
    struct Desc base;

    union {
        char* parseCont;
        Code* body;
        struct Thunk* value;
    };
    char name[];
};

struct RecordEntry {
    struct Desc base;
    int strictness;
    int boxing;
    char** fields;
    char name[];
};

struct PrimEntry {
    struct Desc base;
    int boxingMap;
    void (*exec)(int frame_ptr);
    char name[];
};

void gen_slices(SliceEntry* dest, Desc* forward_ptr, int arity);

void init_desc();

void add_desc(char* fn, Desc* desc);
Desc* find_desc(char* fn);

Desc* get_slice(Desc* f, int nrargs);

int printDesc(Desc* f);

extern struct FunEntry* __INT__;
extern struct FunEntry* __BOOL__;
extern struct FunEntry* __CHAR__;
extern struct FunEntry* __REAL__;

// For constants, always points to the code area
extern struct FunEntry* __STRING_PTR__;
extern struct FunEntry* __ARRAY__; //  Including strings

extern struct FunEntry* __FORWARD_PTR__;

// Function names of the App built in functions, per arity
extern char* appNames[];

#endif // __DESC_H