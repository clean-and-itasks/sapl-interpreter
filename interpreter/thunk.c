#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "debug.h"
#include "thunk.h"
#include "mem.h"
#include "desc.h"

#ifdef DEBUG
int readI(Thunk* thunk) {
    assert(thunk != NULL);
    
    if (thunk->desc != (Desc*) __INT__) {
        printf("readI: not an integer: ");
        printDesc(thunk->desc);
        exit(-1);
    }
    
    return thunk->_int;
}

int readB(Thunk* thunk) {
    assert(thunk != NULL);
    
    if (thunk->desc != (Desc*) __BOOL__) {
        printf("readB: not a boolean: ");
        printDesc(thunk->desc);
        exit(-1);
    }
    
    return thunk->_int;
}

char readC(Thunk* thunk) {
    assert(thunk != NULL);
    
    if (thunk->desc != (Desc*) __CHAR__) {
        printf("readC: not a char: ");
        printDesc(thunk->desc);
        exit(-1);
    }
    
    return (char) thunk->_int;
}

double readR(Thunk* thunk) {
    assert(thunk != NULL);
    
    if (thunk->desc != (Desc*) __REAL__) {
        printf("readR: not a real: ");
        printDesc(thunk->desc);
        exit(-1);
    }
    
    return (char) thunk->_real;
}
#endif
    
bool is_hnf(Thunk* thunk)
{
    return thunk != NULL && thunk->desc->hnf;
}

void print(bool force) {
    Thunk* thunk = pop_a();
    
    while (thunk->desc == (Desc*) __FORWARD_PTR__) {
        thunk = thunk->_forward_ptr;
    }

    putchar('[');

    if (thunk->desc->type == FT_BOXED_LIT) {
        if ((FunEntry*) thunk->desc == __INT__) {
            printf("%d", thunk->_int);
        } else if ((FunEntry*) thunk->desc == __BOOL__) {
            if (thunk->_int) {
                fwrite("True",1,4,stdout);
            } else {
                fwrite("False",1,5,stdout);
            }
        } else if ((FunEntry*) thunk->desc == __CHAR__) {
            putchar('\'');
            putchar((char) thunk->_int);
            putchar('\'');
        } else if ((FunEntry*) thunk->desc == __REAL__) {
            printf("%G", thunk->_real);            
        } else if ((FunEntry*) thunk->desc == __STRING_PTR__) {
            putchar('"');
            fwrite(thunk->_string_ptr->chars,1,thunk->_string_ptr->length,stdout);
            putchar('"');            
        } else if ((FunEntry*) thunk->desc == __ARRAY__) {
                        
            if(thunk->_array.type == AET_CHAR)
            {
                putchar('"');
                fwrite(thunk->_array._chars,1,thunk->_array.length,stdout);
                putchar('"');                
            }
            else
            {
                bool first = true;
                
                putchar('{');                
                for(int i=0; i< thunk->_array.length; i++)
                {
                    if(!first)
                    {
                        putchar(',');
                    }
                    else
                    {
                        first = false;
                    }
                    
                    switch(thunk->_array.type)
                    {
                        case AET_BOOL:
                            if(thunk->_array._bools[i])
                            {
                                fwrite("True",1,4,stdout);
                            }
                            else
                            {
                                fwrite("False",1,5,stdout);
                            }
                            break;
                        case AET_CHAR:
                            putchar(thunk->_array._chars[i]);
                            break;                            
                        case AET_INT:
                            printf("%d", thunk->_array._ints[i]);
                            break;
                        case AET_REAL:
                            printf("%G", thunk->_array._reals[i]);
                            break;
                        case AET_OTHER:
                            push_a(thunk->_array._elems[i]);
                            print(true);
                            break;
                    }
                    
                }                
                putchar('}');            
            }
        } else {
            printf("print: unhandled BOXED LIT\n");
            printDesc(thunk->desc);
            exit(-1);
        }
    } else {
        int arity = printDesc(thunk->desc);

        // Put the arguments on the stack before any "eval", otherwise GC
        // may screws up the pointers
        for (int i = 0; i < arity; i++) {
            push_a(thunk->_args[arity-i-1]);
        }
        
        for (int i = 0; i < arity; i++) {
            putchar(' ');

            if (force) peek_a()->desc->eval();
            print(force);
        }
        
    }

    putchar(']');
}
