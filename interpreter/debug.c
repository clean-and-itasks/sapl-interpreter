#include <stdio.h>
#include <stdlib.h>

#include "debug.h"

#ifdef DEBUG
int gc_cnt = 0;
#endif

void not_implemented(char* msg)
{
    printf("Function not implemented: %s\n", msg);
    exit(-1);
}

void abort(char* msg)
{
    printf("Abort: %s\n", msg);
    exit(-1);
}
