#ifndef __DESC_BASE_H
#define __DESC_BASE_H

enum FunType {
    FT_BOXED_LIT,  // Including arrays
    FT_RECORD, 
    FT_ADT, 
    FT_CAF, 
    FT_CAF_REDUCED, 
    FT_FUN, 
    FT_SLICE, 
    FT_PRIM
};

struct Desc {
    struct 
    {
        FunType type : 4;
        unsigned int arity : 8;         // LIMITATION: maximum 32 arguments
        unsigned int thunk_size : 10;   // It gives false result for strings and arrays
        unsigned int hnf : 1;           // TODO: unused, remove?
        unsigned int unboxable_return : 1;
    };
    
    void (*eval)();
};

#endif // __DESC_H