#ifndef __MEM_H
#define __MEM_H

#include "thunk.h"

#define STACK_SIZE_A 10240*50
#define STACK_SIZE_B 10240*50

#define HEAP_SIZE 20*1024*1024

extern int stack_top_a;
extern int stack_top_b;
extern Thunk* stack_a[STACK_SIZE_A];
extern Thunk  stack_b[STACK_SIZE_B];

extern char* heap_base_curr;
extern char* heap_base_swap;
extern char* heap_curr;
extern char* gc_trigger;

#define peek_a() stack_a[stack_top_a-1]
#define pop_a() stack_a[--stack_top_a]
#define push_a(r) stack_a[stack_top_a++]=(r)

#define local(base, idx) stack_a[base+idx]
#define set_return(base, r) stack_a[base-1]=(r)
#define destroy_stack_frame(base) stack_top_a = base
#define get_dst(base) stack_a[base-1]

#define alloc_b() &stack_b[stack_top_b++]

#define destroy_stack_frame_b(base) stack_top_b = base

void init_mem();
void print_stat();

// TODO: inline
void* alloc_desc(int size);

// TODO: inline
void* alloc_code(int size);

#ifdef DEBUG
void* alloc_heap(int size);
#else
#define alloc_heap(size) heap_curr; heap_curr += size
#endif

#endif // __MEM_H
