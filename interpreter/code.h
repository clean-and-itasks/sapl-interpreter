#ifndef __CODE_H
#define __CODE_H

#include "thunk.h"

enum CodeType {
    CT_VAR,             // on heap
    CT_VAR_STRICT,      // on heap
    CT_VAR_UNBOXED,     // on heap or B stack
    CT_APP_PRIM_FAST,   // primitive functions with 1 or 2 unboxable arg(s))
    CT_APP_PRIM,        // primitive functions with array (string) arg(s)
    CT_APP_THUNK,       // ADT, record, or not saturated app 
    CT_APP_DYN,         // function part is a variable
    CT_APP_FUN, 
    CT_APP_FUN1, 
    CT_APP_FUN2, 
    CT_APP_FUN_TR,      // tail recursive 
    CT_SELECT,          // record field select
    CT_UPDATE,          // record update
    CT_CASE_ADT, 
    CT_CASE_LIT, 
    CT_CASE_STR,
    CT_CASE_REC,      // Record field selection
    CT_IF, 
    CT_LET, 
    CT_THUNK            // constant, always fits the B stack
};

struct Code {
    CodeType type : 5;
    unsigned int nr_args : 5;       // used in AppEntry
    unsigned int nr_cases : 5;      // used in SelectEntry
    unsigned int nr_bindings : 8;   // used in LetEntry
    unsigned int nr_updates : 4;    // used in UpdateEntry    
    unsigned int strict : 1;        // used in VarEntry
    unsigned int arg_pattern : 4;
    void (*create_thunk)(Code*, struct Thunk**, int);
};

struct ThunkEntry {
    struct Code base;
    struct Thunk thunk;
};

struct VarEntry {
    struct Code base;
    int index; // index on the stack
};

struct AppEntry {
    struct Code base;
    union {
        struct VarEntry var;
        struct Desc* f;
    };
    struct Code* args[];
};

struct SelectEntry {
    struct Code base;
    struct Code* expr;
    Thunk idx; 
};

struct UpdateEntry {
    struct Code base;
    struct Code* expr;
    struct OneUpdateEntry* updates[];
};

struct OneUpdateEntry {
    struct Code* expr;
    int idx;
};

struct CaseLitCaseEntry {
    struct Code* body;    
    struct ThunkEntry* lit; // NULL -> default
};

struct CaseEntry {
    struct Code base;
    struct Code* expr;
    
    struct Code* fallback;
    // how many arguments to be removed from the stack in the case of fallback
    int fallback_nrargs;
    // it tells which cases are default. in default cases ADT arguments are omitted
    // from the stack, that's why it is important to know 
    int default_map;
    
    union
    {
        struct CaseLitCaseEntry cases[];
        struct Code* bodies[];
    };
};

struct IfEntry {
    struct Code base;
    struct Code* cond;
    struct Code* texpr;
    struct Code* fexpr;
};

struct LetBindingEntry {
    struct Code* body;
    int type; // 0 - normal, 1 - strict, 2 - unboxable
};

struct LetEntry {
    struct Code base;
    struct Code* body;
    struct LetBindingEntry* bindings[];
};

typedef Thunk* (*create_thunk_fun)(Code*, int);

void set_create_thunk_fun(Code* code);
void set_eval_fun(Desc* desc);

void exec(Code* expr, int frame_ptr, int root_frame_ptr);

#endif // __CODE_H