#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "parse.h"
#include "mem.h"
#include "code.h"
#include "desc.h"
#include "debug.h"

int parseInt(char** ptr, int* result) {
    char *end;
    *result = strtol(*ptr, &end, 10);

    if (*ptr == end) {
        return 0;
    } else {
        *ptr = end + 1;
        return 1;
    }
}

int parseReal(char** ptr, double* result) {
    char *end;
    *result = strtod(*ptr, &end);

    if (*ptr == end) {
        return 0;
    } else {
        *ptr = end + 1;
        return 1;
    }
}

void copyStringAndForward(char* dest, char** source, int length) {
    memcpy(dest, *source, length);
    dest[length] = '\0';
    *source += length;
}

Desc* parseFunName(char** ptr) {
    // This is used to temporarily copy (converts clean string to C style) the function name for lookup
    char name[MAX_IDENTIFIER_LENGTH];

    int nameLength;
    if (!parseInt(ptr, &nameLength)) return 0;

    copyStringAndForward(name, ptr, nameLength);
    Desc* f = (Desc*) find_desc(name);

    if (f == 0) {
        printf("%s not found\n", name);
        return 0;
    }

    return f;
}

int parseDef1(char** ptr) {
    int defSize;
    if (!parseInt(ptr, &defSize)) return -1;

    char* nextDef = *ptr + defSize;

    // 2. Type char
    char type = *(*ptr)++;
    switch (type) {

        case 'F': // Normal function
        {
            // example.g_45 !a_0 !b_1 c_2 = add a_0 b_1
            // 37 F12 example.g_453 6 AF3 add2 VA0 VA1

            // 1. field: length of the name
            int nameLength;
            if (!parseInt(ptr, &nameLength)) return 0;

            // 2. field: characters of the name (postpone copying)
            char* namePtr = *ptr;
            *ptr += nameLength;

            // 3. field: arity
            int arity;
            if (!parseInt(ptr, &arity)) return 0;

            // before the FunEntry there are "arity" number of SliceEntries
            SliceEntry* entry_base = (SliceEntry*) alloc_desc(sizeof (SliceEntry) * arity + sizeof (FunEntry) + nameLength + 1);

            FunEntry* entry = (FunEntry*) (entry_base + arity);
            entry->base.type = FT_FUN;
            entry->base.arity = arity;
            entry->base.thunk_size = thunk_size_f(arity);
            entry->base.hnf = false;
            set_eval_fun((Desc*) entry);
            
            // now the name can be copied into the FunEntry
            memcpy(entry->name, namePtr, nameLength);
            entry->name[nameLength] = '\0';

            // 4. field: strictness bits
            if (!parseInt(ptr, &entry->strictness)) return 0;
            // 5. field: boxing info
            if (!parseInt(ptr, &entry->boxing)) return 0;

            // set the continuation for stage 2
            entry->parseCont = *ptr;

            // generate slices. avoid function call if arity is zero
            if (arity > 0) gen_slices(entry_base, (Desc*) entry, arity);

            add_desc(entry->name, (Desc*) entry);
            break;
        }

        case 'C': // CAF
        {
            // example.f =: example.g_48 1 2
            // 39 C9 example.fAF12 example.g_482 LI1 LI2 

            int nameLength;
            if (!parseInt(ptr, &nameLength)) return 0;

            CAFEntry* entry = (CAFEntry*) alloc_desc(sizeof (CAFEntry) + nameLength + 1);
            entry->base.type = FT_CAF;
            entry->base.arity = 0;
            entry->base.thunk_size = thunk_size_f(0);
            entry->base.hnf = false;
            set_eval_fun((Desc*) entry);
            
            copyStringAndForward(entry->name, ptr, nameLength);

            // set the continuation for stage 2
            entry->parseCont = *ptr;

            add_desc(entry->name, (Desc*) entry);
            break;
        }

        case 'A': // ADT
        {
            // :: example.A = example.A !a1 a2 | example.B 
            // 33 A2 9 example.A2 2 9 example.B0 0 

            // 1. field: the number of constructors
            int conNum;
            if (!parseInt(ptr, &conNum)) return 0;

            for (int i = 0; i < conNum; i++) {
                // 1. field: length of the name
                int nameLength;
                if (!parseInt(ptr, &nameLength)) return 0;

                // 2. field: characters of the name (postpone copying)
                char* namePtr = *ptr;
                *ptr += nameLength;

                // 3. field: arity
                int arity;
                if (!parseInt(ptr, &arity)) return 0;

                // before the FunEntry there are "arity" number of SliceEntries
                SliceEntry* entry_base = (SliceEntry*) alloc_desc(sizeof (SliceEntry) * arity + sizeof (ADTEntry) + nameLength + 1);

                ADTEntry* entry = (ADTEntry*) (entry_base + arity);
                entry->base.type = FT_ADT;
                entry->base.arity = arity;
                entry->base.thunk_size = thunk_size_f(arity);
                entry->base.hnf = true;
                entry->idx = i;
                entry->nrConses = conNum;
                set_eval_fun((Desc*) entry);                
                
                // now the name can be copied into the ADTEntry
                memcpy(entry->name, namePtr, nameLength);
                entry->name[nameLength] = '\0';

                // 4. field: strictness bits
                if (!parseInt(ptr, &entry->strictness)) return 0;
                // 5. field: boxing info
                if (!parseInt(ptr, &entry->boxing)) return 0;

                // generate slices. avoid function call if arity is zero
                if (arity > 0) gen_slices(entry_base, (Desc*) entry, arity);

                add_desc(entry->name, (Desc*) entry);
            }

            break;
        }

        case 'R': // Record
        {
            // :: example._R = {example.a, !example.b}
            // 40 R10 example._R2 1 9 example.a9 example.b

            int nameLength;
            if (!parseInt(ptr, &nameLength)) return 0;

            RecordEntry* entry = (RecordEntry*) alloc_desc(sizeof (RecordEntry) + nameLength + 1);
            entry->base.type = FT_RECORD;

            copyStringAndForward(entry->name, ptr, nameLength);

            int arity;
            if (!parseInt(ptr, &arity)) return 0;
            entry->base.arity = arity;
            if (!parseInt(ptr, &entry->strictness)) return 0;
            if (!parseInt(ptr, &entry->boxing)) return 0;
            
            entry->base.thunk_size = thunk_size_f(arity);            
            entry->base.hnf = true;
            set_eval_fun((Desc*) entry);
            
            entry->fields = (char**) alloc_desc(sizeof (char*) * arity);
            for (int i = 0; i < arity; i++) {
                int fieldNameLength;
                if (!parseInt(ptr, &fieldNameLength)) return 0;

                entry->fields[i] = (char*) alloc_desc(fieldNameLength + 1);                
                copyStringAndForward(entry->fields[i], ptr, fieldNameLength);
            }

            add_desc(entry->name, (Desc*) entry);
            break;
        }

        default:
            return 0;
    }

    *ptr = nextDef;
    return 1;
}

ThunkEntry* parseLit(char **ptr) {
    // 1. Type char
    char type = *(*ptr)++;

    struct ThunkEntry* entry = (ThunkEntry*) alloc_code(sizeof (ThunkEntry));
    entry->base.type = CT_THUNK;
    
    switch (type) {
        case 'I': // Int
        {
            entry->thunk.desc = (Desc*) __INT__;
            if (!parseInt(ptr, &entry->thunk._int)) return 0;
            break;
        }

        case 'C': // Char
        {
            entry->thunk.desc = (Desc*) __CHAR__;
            entry->thunk._int = *(*ptr)++;
            break;
        }

        case 'R': // Real
        {
            entry->thunk.desc = (Desc*) __REAL__;
            if (!parseReal(ptr, &entry->thunk._real)) return 0;
            break;
        }

        case '0': // Bool
        case '1':
        {
            entry->thunk.desc = (Desc*) __BOOL__;
            entry->thunk._int = type == '1';
            break;
        }

        case 'S': // String
        {
            int strlen = 0;
            if (!parseInt(ptr, &strlen)) return 0;
            
            CleanString* strptr = (CleanString*) alloc_code(sizeof (CleanString) + strlen);
            strptr->length = strlen;
            memcpy(&strptr->chars, *ptr, strlen);
            *ptr += strlen;
            
            entry->thunk.desc = (Desc*) __STRING_PTR__;
            entry->thunk._string_ptr = strptr;       
            break;
        }
    }

    set_create_thunk_fun((Code*) entry);
    return entry;
}

VarEntry* parseVar(char **ptr, VarEntry* target) {
    // 1. Type char
    char type = *(*ptr)++;

    struct VarEntry* entry = target == NULL ? (VarEntry*) alloc_code(sizeof (VarEntry)) : target;    
    
    switch (type) {
        case 'L': // Local var
            entry->base.type = CT_VAR;
            entry->base.strict = false;
            break;
        case 'S': // Strict local var    
            entry->base.type = CT_VAR_STRICT;
            entry->base.strict = true;
            break;
        case 'U': // Strict unboxable var    
            entry->base.type = CT_VAR_UNBOXED;
            entry->base.strict = true;
            break;            
        default:
            return 0;
    }
    
    if (!parseInt(ptr, &entry->index)) return 0;
    set_create_thunk_fun((Code*) entry);
    return entry;    
}

Code* parseTerm(char **ptr);
Code* parseFallbackBody(char **ptr, Code* fallback, int fallback_nrargs);

/* 
 * It is very messy, because it handles 3 cases:
 * 1. Dynamic app: the function part is a local variable or argument  
 * 2. Static app, where the function is a zero arg data constructor, or a non-zero parameter function with zero arguments
 * 3. Tail recursive
 * 4. Other static cases
 */
Code* parseApp(char **ptr, bool dynamic, bool tr) {

    int nrArgs;
    if (!parseInt(ptr, &nrArgs)) return 0;
    
    Desc* desc = NULL;
    bool overSaturated = false;
    
    if(!dynamic)
    {
        (*ptr)++; // Skip 'F' for the static var
        
        desc = parseFunName(ptr); // can fail
        overSaturated = desc->arity < nrArgs;
    }
    
    int nrArgsToParse = overSaturated ? desc->arity : nrArgs;
    
    struct AppEntry* entry = NULL;
    
    if(!dynamic && nrArgs == 0)
    {
        if(desc != NULL) desc = get_slice(desc, nrArgs);

        if(desc != NULL && (desc->type == FT_ADT || desc->type == FT_SLICE))
        {
            struct ThunkEntry* entry = (ThunkEntry*) alloc_code(sizeof (ThunkEntry));
            entry->base.type = CT_THUNK;
            entry->thunk.desc = desc;

            set_create_thunk_fun((Code*) entry);
            return (Code*) entry;
        }
        
        entry = (AppEntry*) alloc_code(sizeof (AppEntry));        
        entry->f = desc;

        // TODO: CAS
        entry->base.type = CT_APP_FUN;
    }
    else
    {
        entry = (AppEntry*) alloc_code(sizeof (AppEntry) + sizeof (void*) * nrArgsToParse);

        if(dynamic)
        {
            parseVar(ptr, &entry->var);        
            entry->base.type = CT_APP_DYN;
        }
        
        for (int i = 0; i < nrArgsToParse; i++) {
            entry->args[i] = parseTerm(ptr);
            if (entry->args[i] == 0) return 0;
        }        
        
        if(!dynamic)
        {
            if(desc != NULL) desc = get_slice(desc, nrArgsToParse);
            entry->f = desc;

            if(desc->type == FT_PRIM && desc->arity == 1 && ((PrimEntry*) desc)->boxingMap == 0b1)
            {
                int arg0strict = entry->args[0]->type == CT_VAR_STRICT || entry->args[0]->type == CT_VAR_UNBOXED;
                
                entry->base.type = CT_APP_PRIM_FAST;
                
                if(arg0strict)
                {
                    entry->base.arg_pattern = 8;
                }
                else if(entry->args[0]->type == CT_THUNK)
                {
                    entry->base.arg_pattern = 9;
                }
                else
                {
                    entry->base.arg_pattern = 10;
                }
            }
            else if(desc->type == FT_PRIM && desc->arity == 2 && ((PrimEntry*) desc)->boxingMap == 0b11)
            {
                int arg0strict = entry->args[0]->type == CT_VAR_STRICT || entry->args[0]->type == CT_VAR_UNBOXED;
                int arg1strict = entry->args[1]->type == CT_VAR_STRICT || entry->args[1]->type == CT_VAR_UNBOXED;
                
                entry->base.type = CT_APP_PRIM_FAST;
                
                if(arg0strict && entry->args[1]->type == CT_THUNK)
                {
                    entry->base.arg_pattern = 1;
                }
                else if(entry->args[0]->type == CT_THUNK && arg1strict)
                {
                    entry->base.arg_pattern = 2;
                }                
                else if(arg0strict && arg1strict)
                {
                    entry->base.arg_pattern = 3;
                }                
                else if(entry->args[0]->type == CT_THUNK)
                {
                    entry->base.arg_pattern = 4;
                }                                
                else if(entry->args[1]->type == CT_THUNK)
                {
                    entry->base.arg_pattern = 5;
                }                                
                else if(arg0strict)
                {
                    entry->base.arg_pattern = 6;
                }                                                                
                else if(arg1strict)
                {
                    entry->base.arg_pattern = 7;
                }                                                
                else
                {
                    entry->base.arg_pattern = 0;
                }
            }            
            else if(desc->type == FT_PRIM)
            {
                entry->base.type = CT_APP_PRIM;
            }
            else if(desc->type == FT_FUN && !tr)
            {
                if(nrArgsToParse == 1)
                {
                    entry->base.type = CT_APP_FUN1;
                }
                else if(nrArgsToParse == 2)
                {
                    entry->base.type = CT_APP_FUN2;
                }
                else
                {
                    entry->base.type = CT_APP_FUN;                   
                }
            }
            else if(desc->type == FT_FUN && tr)
            {
                entry->base.type = CT_APP_FUN_TR;
            }
            else
            {
                entry->base.type = CT_APP_THUNK;
            }
        }        
    }
    
    entry->base.nr_args = nrArgsToParse;        
    set_create_thunk_fun((Code*) entry);
    
    // Create a wrapping Appx if necessary
    if(overSaturated)
    {
        int appArity = nrArgs - desc->arity;
        assert(appArity <= 9);
        
        struct AppEntry* appEntry 
            = (AppEntry*) alloc_code(sizeof (AppEntry) + sizeof (void*) * (appArity + 1));
        
        appEntry->f = (Desc*) find_desc(appNames[appArity-1]);
        appEntry->base.type = CT_APP_FUN;
        appEntry->base.nr_args = appArity + 1;
        set_create_thunk_fun((Code*) appEntry);
        
        appEntry->args[0] = (Code*) entry;
        
        for (int i = 1; i <= appArity; i++) {
            appEntry->args[i] = parseTerm(ptr);
            if (appEntry->args[i] == 0) return 0;
        }     
                
        assert(appEntry->f != NULL);
        
        return (Code*) appEntry;
    }
    else
    {        
        return (Code*) entry;
    }
}

CaseEntry* parseCase(char **ptr, Code* fallback, int fallback_nrargs) {
    Code* expr = parseTerm(ptr);
    
    int nrCases;
    if (!parseInt(ptr, &nrCases)) return 0;
    
    struct CaseEntry* entry = NULL;    
    
    char type = **ptr;

    bool isDefault = false;
    Code* defaultBody = NULL;
    
    // Default is always the first
    if(type == 'D')
    {
        isDefault = true;
        (*ptr)++;
        
        defaultBody = (Code*) parseFallbackBody(ptr, fallback, fallback_nrargs);           
        type = **ptr;        
    }

    // If there is no default here, use the parent fallback
    Code* child_fallback = isDefault ? defaultBody : fallback;
    int child_fallback_base_nrargs = isDefault ? 0 : fallback_nrargs;
    
    bool isADT = type == 'C';
    
    if(isADT)
    {
        (*ptr)++;
                
        int nrConses = 1;
        
        Desc* firstCase = (Desc*) parseFunName(ptr);
        
        if(firstCase->type == FT_ADT)
        {
            nrConses = ((ADTEntry*) firstCase)->nrConses;
        }
        
        Code* firstBody = (Code*) parseFallbackBody(ptr, child_fallback, 
            child_fallback_base_nrargs + firstCase->arity);   
        
        entry = (CaseEntry*) alloc_code(sizeof (CaseEntry) + sizeof (Code*) * nrConses);
        
        if(firstCase->type == FT_ADT)
        {
            entry->base.type = CT_CASE_ADT;
            entry->base.nr_cases = nrConses;
            entry->default_map = 0xFFFFFFFF;
            
            // set the default case for all the entries
            for (int i = 0; i < nrConses; i++) {
                entry->bodies[i] = defaultBody;
            }

            if(isDefault) nrCases--;

            nrCases--; // firstCase
            entry->bodies[((ADTEntry*)firstCase)->idx] = firstBody;
            entry->default_map &= ~(1<<((ADTEntry*)firstCase)->idx);
            
            for (int i = 0; i < nrCases; i++) {
                (*ptr)++; // skip type
                ADTEntry* nextCase = (ADTEntry*) parseFunName(ptr);
                entry->bodies[nextCase->idx] = (Code*) parseFallbackBody(ptr, child_fallback, 
                    child_fallback_base_nrargs + nextCase->base.arity);
                entry->default_map &= ~(1<<nextCase->idx);
            }
        }
        else
        {
            entry->base.type = CT_CASE_REC;
            entry->base.nr_cases = 1;
            entry->bodies[0] = firstBody;
        }
    }
    else
    {
        entry = (CaseEntry*) alloc_code(sizeof (CaseEntry) + sizeof (CaseLitCaseEntry) * nrCases);
        entry->base.type = CT_CASE_LIT;
        entry->base.nr_cases = nrCases;
        
        if(isDefault)
        {
            nrCases--;
            entry->cases[nrCases].body = defaultBody;
            entry->cases[nrCases].lit = NULL;
        }
        
        for (int i = 0; i < nrCases; i++) {
            (*ptr)++; // skip type
            entry->cases[i].lit = parseLit(ptr);
            
            // String literal is the third case for efficiency reasons
            if(i == 0 && entry->cases[i].lit->thunk.desc == (Desc*) __STRING_PTR__) 
            {
                entry->base.type = CT_CASE_STR;
            }
            
            entry->cases[i].body = (Code*) parseFallbackBody(ptr, child_fallback, child_fallback_base_nrargs);
        }        
    }

    entry->expr = expr;
    entry->fallback = fallback;
    entry->fallback_nrargs = fallback_nrargs;
    
    set_create_thunk_fun((Code*) entry);
    return entry;
}

IfEntry* parseIf(char **ptr, Code* fallback, int fallback_nrargs) {
    struct IfEntry* entry = (IfEntry*) alloc_code(sizeof (IfEntry));
    entry->base.type = CT_IF;
    entry->cond = parseTerm(ptr);
    entry->texpr = parseFallbackBody(ptr, fallback, fallback_nrargs);
    entry->fexpr = parseFallbackBody(ptr, fallback, fallback_nrargs);

    set_create_thunk_fun((Code*) entry);    
    return entry;
}

Code* parseFallbackBody(char **ptr, Code* fallback, int fallback_nrargs) {

    char type = **ptr;
    
    if(type == 'C')
    {
        (*ptr)++;
        return (Code*) parseCase(ptr, fallback, fallback_nrargs);
    }
    else if(type == 'I')
    {
        (*ptr)++;
        return (Code*) parseIf(ptr, fallback, fallback_nrargs);
    }
    else
    {
        return parseTerm(ptr);
    }
}

SelectEntry* parseSelect(char **ptr) {
    
    struct SelectEntry* entry = (SelectEntry*) alloc_code(sizeof (SelectEntry));
    entry->expr = parseTerm(ptr);
    entry->base.type = CT_SELECT;
    entry->idx.desc = (Desc*) __INT__;
    if (!parseInt(ptr, &entry->idx._int)) return 0;
    
    set_create_thunk_fun((Code*) entry);
    return entry;
}

UpdateEntry* parseUpdate(char **ptr) {
    
    Code* recExpr = parseTerm(ptr);

    int nrUpdates;
    if (!parseInt(ptr, &nrUpdates)) return 0;

    struct UpdateEntry* entry = (UpdateEntry*) alloc_code(sizeof (UpdateEntry) + sizeof (OneUpdateEntry*) * nrUpdates);    
    
    entry->expr = recExpr;
    entry->base.type = CT_UPDATE;
    entry->base.nr_updates = nrUpdates;
    
    for(int i=0; i<nrUpdates; i++)
    {
        struct OneUpdateEntry* upd = (OneUpdateEntry*) alloc_code(sizeof (OneUpdateEntry));
        
        if (!parseInt(ptr, &upd->idx)) return 0;
        upd->expr = parseTerm(ptr);
        
        entry->updates[i] = upd;
    }
    
    set_create_thunk_fun((Code*) entry);
    return entry;
}

LetEntry* parseLet(char **ptr) {
    Code* body = parseTerm(ptr);
    
    int nrBindings;
    if (!parseInt(ptr, &nrBindings)) return 0;
        
    struct LetEntry* entry = (LetEntry*) alloc_code(sizeof (LetEntry) + sizeof (Code*) * nrBindings);
    entry->base.type = CT_LET;   
    entry->base.nr_bindings = nrBindings;
    entry->body = body;
  
    for(int i=0; i<nrBindings; i++)
    {
        struct LetBindingEntry* binding = (LetBindingEntry*) alloc_code(sizeof (LetBindingEntry));

        char typeChar = *(*ptr)++;

        switch (typeChar) {
            case 'L': // Local var
                binding->type = 0;
                break;
            case 'S': // Strict local var    
                binding->type = 1;
                break;
            case 'U': // Strict unboxable var    
                binding->type = 2;
                break;            
            default:
                return 0;
        }

        binding->body =  parseTerm(ptr);
        entry->bindings[i] = binding;
    }

    set_create_thunk_fun((Code*) entry);    
    return entry;
}

Code* parseTerm(char **ptr) {
    // 1. Type char
    char type = *(*ptr)++;

    switch (type) {
        case 'L': // Literal
            return (Code*) parseLit(ptr);
        case 'V': // Variable
            return (Code*) parseVar(ptr, NULL);
        case 'A': // Static application
            return (Code*) parseApp(ptr, false, false);
        case 'T': // Tail recursive application
            return (Code*) parseApp(ptr, false, true);
        case 't': // Tail call application
            return (Code*) parseApp(ptr, false, true);
        case 'D': // Dynamic application
            return (Code*) parseApp(ptr, true, false);
        case 'S': // Select
            return (Code*) parseSelect(ptr);
        case 'U': // Update
            return (Code*) parseUpdate(ptr);
        case 'C': // Case
            return (Code*) parseCase(ptr, NULL, 0);
        case 'I': // If
            return (Code*) parseIf(ptr, NULL, 0);
        case 'E': // Let
            return (Code*) parseLet(ptr);            
        default:
            printf("parseTerm: unhandled term type\n");
            exit(-1);
    }
}

int parseDef2(char** ptr) {
    int defSize;
    if (!parseInt(ptr, &defSize)) return -1;

    char* nextDef = *ptr + defSize;

    // 2. Type char
    char type = *(*ptr)++;

    switch (type) {
        case 'F': // Normal function
        {
            FunEntry* entry = (FunEntry*) parseFunName(ptr); // should not fail, just added to the map in the 1. phase

            // read continuation
            *ptr = entry->parseCont;

            // parse body
            entry->body = parseTerm(ptr);

            break;
        }

        case 'C': // CAF
        {
            CAFEntry* entry = (CAFEntry*) parseFunName(ptr); // should not fail, just added to the map in the 1. phase

            // read continuation
            *ptr = entry->parseCont;

            // parse body
            entry->body = parseTerm(ptr);

            break;
        }

        case 'A':
        case 'R':
            break; // skip it, already fully parsed

        default:
            return -1;
    }

    *ptr = nextDef;
    return 1;
}

int parse(char** ptr, int length) {
    char* origptr = *ptr;

    char* endptr = *ptr + length;
    int numDefs = 0;

    while (*ptr < endptr) {
        numDefs++;
        if (parseDef1(ptr) < 0) return -1;
    }

    *ptr = origptr;

    while (*ptr < endptr) {
        if (parseDef2(ptr) < 0) return -1;
    }

    return numDefs;
}


