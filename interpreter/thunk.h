#ifndef __THUNK_H
#define __THUNK_H

#include "debug.h"
#include "desc_base.h"

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#pragma pack(push, 1)

struct Thunk;

typedef struct __attribute__((packed)) CleanString {
    int length;
    char chars[];
} CleanString;

enum ArrayElementType {
    AET_BOOL,
    AET_INT,
    AET_CHAR,
    AET_REAL,
    AET_OTHER
};

typedef struct __attribute__((packed)) Array {
    struct
    {
        ArrayElementType type : 3;
        unsigned int is_boxed : 1;
        unsigned int bytes_per_elem : 3;
        unsigned int length : 25; // LIMITATION :)
    };
    union
    {
        char _chars[];
        int _ints[];
        double _reals[];
        unsigned char _bools[];
        Thunk* _elems[];  
    };    
} Array;

typedef struct __attribute__((packed)) Thunk {
    struct Desc* desc;

    union {
        Thunk* _forward_ptr;
        int _int;               // also char and bool 
        double _real;           
        struct CleanString* _string_ptr; // For CT_THUNK
        struct Array _array;
        Thunk* _args[];
    };
} Thunk;

#pragma pack(pop)

#define thunk_size_f(arity) ((max(sizeof (Thunk), sizeof (Desc*) + sizeof (Thunk*) * arity) + 3) / 4) * 4

#ifdef DEBUG

int readI(Thunk* thunk);
int readB(Thunk* thunk);
char readC(Thunk* thunk);
double readR(Thunk* thunk);

#else

#define readI(thunk) thunk->_int
#define readB(thunk) thunk->_int
#define readC(thunk) thunk->_int
#define readR(thunk) thunk->_real

#endif

bool is_hnf(Thunk* thunk);

// Thunk is supposed to be in HNF on the A stack
void print(bool force);

#endif // __THUNK_H