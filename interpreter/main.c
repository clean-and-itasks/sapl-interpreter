#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/time.h>

#include "debug.h"
#include "desc.h"
#include "prim.h"
#include "parse.h"
#include "code.h"
#include "mem.h"

long readfile(char **string, FILE *f) 
{
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    *string = (char*) malloc(fsize);
    fread(*string, fsize, 1, f);
    fclose(f);

    return fsize;
}

int loadFile(char* input, char** content)
{    
    FILE* file = fopen(input, "r");
    long len = 0;

    if ( file == 0 )
    {
        return -1;
    }
    else 
    {	        
        len = readfile(content, file);
                
        if(len < 0)
        {
            printf( "No Input\n" );
        }
			
        fclose( file );
        if(len < 0) return -1;
    }
    
    return len;
}

int main ( int argc, char *argv[] )
{
    init_mem();
    init_desc();
    init_prim();
 
#ifdef DEBUG
    printf("sizeof(int): %d, sizeof(long): %d, sizeof(void*): %d, sizeof(Thunk): %d\n\n", 
            sizeof(int), sizeof(long), sizeof(void*), sizeof(Thunk));
#endif
    
    char* line = NULL;
    long len = 0;
    
    char* input = "c:\\Users\\Laszlo\\Documents\\personal\\projects\\sapl-interpreter\\interpreter\\builtin.bsapl";

    len = loadFile(input, &line);
    if(len <= 0)
    {
        printf( "Could not load builtin.bsapl\n" );
        exit(-1);        
    }
    
    parse(&line, len);
    
    input = "..\\tests\\jurrien.bsapl";
    
    if ( argc == 2 )
    {
        input = argv[1];
    }

    len = loadFile(input, &line);
    if(len <= 0)
    {
        printf( "Could not open file\n" );
        exit(-1);        
    }
        
    int nrfuns = parse(&line, len);

#ifdef DEBUG
    printf("Number of functions parsed: %d\n", nrfuns);
#endif

    if(nrfuns<=0)
    {
        exit(0);
    }
    
    // TODO: put it into a special "expression" space, instead of "code"
    char *exprstream = "A0 F4 main";

    Code* expr = parseTerm(&exprstream);
    
#ifdef BENCHMARK
    struct timeval t1, t2;
    gettimeofday(&t1, NULL);
#endif

    push_a(NULL);
    exec(expr, stack_top_a, stack_top_a);    

    print(true);
    
#ifdef BENCHMARK
    gettimeofday(&t2, NULL);
#endif

#ifdef BENCHMARK
    // compute and print the elapsed time in millisec
    double elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms    
    printf("\n\nExecution time: %G ms\n", elapsedTime);
#endif
    
#ifdef DEBUG    
    printf("\nGC count: %i\n", gc_cnt);
    print_stat();
#endif
    
    exit(0);
}
