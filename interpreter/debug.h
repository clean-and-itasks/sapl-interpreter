#ifndef DEBUG_H
#define	DEBUG_H

#define DEBUG
//#define DEBUG_EXEC
//#define DEBUG_GC
#define BENCHMARK

#ifndef DEBUG
#define NDEBUG
#endif

#include <assert.h>

void not_implemented(char* msg);
void abort(char* msg);

#ifdef DEBUG
extern int gc_cnt;
#endif

#endif	/* DEBUG_H */

