#include <stdlib.h>
#include <stdio.h>

#include "debug.h"
#include "mem.h"
#include "gc.h"

int desc_alloc;
int code_alloc;
int nr_heap_alloc;
int heap_alloc;

int stack_top_a;
int stack_top_b;
Thunk* stack_a[STACK_SIZE_A];
Thunk  stack_b[STACK_SIZE_B];

char* heap_base_curr;
char* heap_base_swap;
char* heap_curr;
char* gc_trigger;

void print_stat() {
    printf("\nallocation:\n");
    printf("desc: %d\n", desc_alloc);
    printf("code: %d\n", code_alloc);
    printf("heap: %d (%d thunks)\n", heap_alloc, nr_heap_alloc);
}

void init_mem() {
    desc_alloc = 0;
    code_alloc = 0;
    nr_heap_alloc = 0;
    
    stack_top_a = 0;
    
    heap_base_curr = (char*) malloc(HEAP_SIZE);
    heap_base_swap = (char*) malloc(HEAP_SIZE);

    assert(heap_base_curr != NULL);
    assert(heap_base_swap != NULL);
    
    heap_curr = heap_base_curr;
    
    gc_trigger = heap_base_curr + HEAP_SIZE - 1024;
}

void* alloc_desc(int size) {
    size = ((size + 3) / 4) * 4;
    desc_alloc += size;
    
    void* ret = malloc(size);
    assert(ret != NULL);
    return ret;
}

void* alloc_code(int size) {
    code_alloc += ((size + 3) / 4) * 4;

    void* ret = malloc(size);
    assert(ret != NULL);
    return ret;
}

#ifdef DEBUG
void* alloc_heap(int size) {
                
    char* curr = heap_curr;
    heap_curr += size;
  
    nr_heap_alloc++;
    heap_alloc+=size;
    
    assert(heap_curr - heap_base_curr < HEAP_SIZE);
    return curr;
}
#endif