module precompiler

import Sapl.SaplParser
import Sapl.SaplTokenizer
import Sapl.Transform.Let
import Sapl.Optimization.StrictnessPropagation
import Sapl.Transform.AddSelectors
import Lifting, Prims
import StdBool, StdList, StdOrdList, StdFile, StdFunc, StdArray, StdDebug

import Text.StringAppender, Text 
import Data.Map

import Text.Unicode.Encodings.JS
from Text.Unicode.UChar import instance toChar UChar, instance toInt UChar

import System.CommandLine
import System.File


:: TypeInfo = Normal | Strict | UnBoxable 
:: VarType = Local Int TypeInfo

:: Context = { vars       :: Map String VarType
			 , localcount :: Int
			 , inspine    :: Bool
			 , currentFun :: String
			 }

// Fusion of function applications for some very basic cases
simplify (SApplication (SVar var1) [SApplication (SVar var2) args]) | unpackVar var1 == "not" && unpackVar var2 == "eqI"
	= SApplication (SVar (NormalVar "neqI" 0)) args
simplify (SApplication (SVar var1) [SApplication (SVar var2) args]) | unpackVar var1 == "not" && unpackVar var2 == "ltI"
	= SApplication (SVar (NormalVar "geI" 0)) args
simplify x = x

unBoxableType (Type "I") = True
unBoxableType (Type "C") = True
unBoxableType (Type "B") = True
unBoxableType (Type "R") = True
unBoxableType _ = False

selectCaseOrder (PDefault,_) _ = True
selectCaseOrder _ _ = False

newContext = {vars = newMap, localcount = 0, inspine = False, currentFun = ""}

typeInfo (TypedVar (StrictVar _ _) type) | unBoxableType type
	= UnBoxable
	= Strict
	
typeInfo _ = Normal

registerVars vars idx [] = vars
registerVars vars idx [v:vs] = registerVars (put (unpackVar v) (Local idx (typeInfo v)) vars) (idx + 1) vs

calcStrictness [] _ = 0
calcStrictness [TypedVar (StrictVar _ _) _:vs] idx = (1 << idx) + calcStrictness vs (idx + 1)
calcStrictness [_:vs] idx = calcStrictness vs (idx + 1)

calcBoxing [] _ = 0
calcBoxing [TypedVar (StrictVar _ _) type:vs] idx | unBoxableType type = (1 << idx) + calcBoxing vs (idx + 1)
calcBoxing [_:vs] idx = calcBoxing vs (idx + 1)

sFunc ctx (FTFunc name body params) a 
	# ctx = {ctx & vars = registerVars ctx.vars 0 params, localcount = length params, inspine = True, currentFun = (unpackVar name)}
	= a <++ "F" <++ sText (unpackVar name) <++ sNum (length params) 
	<++ sNum (calcStrictness params 0) <++ sNum (calcBoxing params 0) 
	<++ sTerm ctx (addSelectors body)

sFunc ctx (FTCAF name body) a 
	# ctx = {ctx & inspine = False, currentFun = (unpackVar name)}
	= a <++ "C" <++ sText (unpackVar name) <++ sTerm ctx body

sFunc ctx (FTRecord name fields) a 
	# ctx = {ctx & inspine = False, currentFun = (unpackVar name)}
	= a <++ "R" <++ sText (unpackVar name) <++ sNum (length fields) 
	    <++ sNum (calcStrictness fields 0) <++ sNum (calcBoxing fields 0) 
	    <++ sList0 sText (map unpackVar fields)

sFunc ctx (FTADT typeName cs) a
	# ctx = {ctx & inspine = False, currentFun = (unpackVar typeName)}
	= a <++ "A" <++ sList sCon cs	
where
	sCon (SaplConstructor name _ params) a
		= a <++ sText (unpackVar name) <++ sNum (length params) <++ sNum (calcStrictness params 0) <++ sNum (calcBoxing params 0)

sList f es a = a <++ sNum (length es) <++ sList0 f es 
sList0 f [e] a = a <++ f e
sList0 f [e:es] a = sList0 f es (a <++ f e <++ "")
sList0 f [] a = a	

sNum num a = a <++ num <++ " "
sText text a = a <++ sNum (textSize text) <++ text

sOneUpdate ctx (idx, expr) a = a <++ sNum idx <++ sTerm ctx expr

sTerm ctx t a = sTermS ctx (simplify t) a 
where
	sTermS ctx (SLit lit) a = a <++ "L" <++ lit
	sTermS ctx (SVar var) a = a <++ sVarApp ctx var
	sTermS ctx (SSelect expr _ idx) a = a <++ "S" <++ sTerm {ctx & inspine = False} expr <++ sNum idx
	sTermS ctx (SUpdate expr _ upds) a = a <++ "U" <++ sTerm {ctx & inspine = False} expr <++ sList (sOneUpdate ctx) upds
	// The function part could by arbitrary SaplTerm in theory, but in practice at this point
	// it can be only SVar
	sTermS ctx (SApplication (SVar var) terms) a = a <++ appType ctx var <++ sNum (length terms) <++ sVar ctx var <++ sList0 (sTerm {ctx & inspine = False}) terms
	sTermS ctx (SCase cond [(PLit (LBool True), texpr), (PLit (LBool False), fexpr)]) a =  a <++ "I" <++ sTerm {ctx & inspine = False, localcount = ctx.localcount + 1} cond <++ sTerm ctx texpr <++ sTerm ctx fexpr
	sTermS ctx (SCase cond [(PLit (LBool False), fexpr), (PLit (LBool True), texpr)]) a =  a <++ "I" <++ sTerm {ctx & inspine = False, localcount = ctx.localcount + 1} cond <++ sTerm ctx texpr <++ sTerm ctx fexpr
	sTermS ctx (SCase expr cs) a = a <++ "C" <++ sTerm {ctx & inspine = False, localcount = ctx.localcount + 1} expr <++ sList (sSelectCase ctx) (sortBy selectCaseOrder cs)
	sTermS ctx (SLet body bindings) a 
		# bindings = fromJust (sortBindings bindings)
		# ctx = {ctx & vars = registerVars ctx.vars ctx.localcount (map unpackBindVar bindings), localcount = ctx.localcount + length bindings}
		= a <++ "E" <++ sTerm ctx body <++ sList (sLetDef {ctx & inspine = False}) bindings

isLocalVar ctx var = member (unpackVar var) ctx.vars

appType ctx var | isLocalVar ctx var 
  = "D"
    
appType {inspine = True, currentFun} var = if (unpackVar var == currentFun) "T" "A" // T: tail recursive / TODO: "t: tail call "
appType _ _ = "A" // A: normal call
    
sLetDef ctx (SaplLetDef var binding) a = a <++ sVarFlag ctx (Local 0 (typeInfo var)) <++ sTerm ctx binding
  
// TODO: find constructor for strictness info
sSelectCase ctx (PCons varName params, expr) a
	# ctx = {ctx & vars = registerVars ctx.vars ctx.localcount (map (\p->TypedVar p NoType) params), localcount = ctx.localcount + length params}
	= a <++ "C" <++ sText varName <++ sTerm ctx expr
sSelectCase ctx (PLit lit, expr) a
	= a <++ "L" <++ lit <++ sTerm ctx expr
sSelectCase ctx (PDefault, expr) a
	= a <++ "D" <++ sTerm ctx expr

sVarFlag ctx var a
	= case var of
		(Local i Strict)    = a <++ "S"
		(Local i UnBoxable) = a <++ "U"		
		(Local i Normal)    = a <++ "L"

sVar ctx var a
	= case get varName ctx.vars of
		(Just l=:(Local i _)) = a <++ sVarFlag ctx l <++ sNum i
		_				      = a <++ "F" <++ sText varName
where
	varName = unpackVar var

sVarApp ctx var a
	= case get varName ctx.vars of
		(Just l=:(Local i _)) = a <++ "V" <++ sVarFlag ctx l <++ sNum i
		_				      = a <++ "A" <++ sNum 0 <++ sVar ctx var <++ sList0 (sTerm ctx) []
where
	varName = unpackVar var

genDefs [] a = a
genDefs [f:fs] a = a <++ textSize fstr <++ " " <++ fstr <++ genDefs fs 
where
	fstr = toString (sFunc newContext f newAppender)

encodeString :: UString -> String
encodeString us = {c \\ c <- map (convert o toInt) us}
where
	convert cc 
		| cc > 255 = '?'
		= toChar cc		

instance Appendable Literal  
where
	(<++) a (LString lit) = a <++ "S" <++ sText (encodeString lit)
	(<++) a (LInt lit) = a <++ "I" <++ sNum lit
	(<++) a (LReal lit) = a <++ "R" <++ sNum lit
	(<++) a (LChar [c]) = a <++ "C" <++ toString (toChar c)
	(<++) a (LBool True) = a <++ "1"
	(<++) a (LBool False) = a <++ "0"

isStrictArg :: !ParserState !String !Int !Int -> Bool
isStrictArg {ps_constructors, ps_functions} n nr_args i
	= checkCons
where
	checkCons = case get n ps_constructors of
					(Just cons) = if (nr_args < cons.nr_args || i >= cons.nr_args) False (isStrictVar (cons.args !! i))
								= checkFun 
								
	checkFun = case get n ps_functions of
					(Just args) = let largs = length args in if (nr_args < largs || i >= largs) False (isStrictVar (args !! i))
								= checkPrim
								
	checkPrim =  case get n primMap of
					(Just arity) = if (nr_args < arity || i >= arity) False True
							   	= False

Start world
	# (args, world) = getCommandLine world
	
	| length args < 2 
		= setReturnCode -1 world
	# no_strictness_prop = args!!1 == "--no-strictness-propagation"
	# extra = if no_strictness_prop 1 0

	| length args < (3 + extra) 
		= setReturnCode -1 world
	# (Ok sapl, world) = readFile (args!!(1+extra)) world
	
	// tokenize
	# tokens = tokensWithPositions sapl
	// parse
	# (Ok (fs, ps)) = parse tokens
	// strictness propagation
	# (fs, ps) = if no_strictness_prop (fs, ps) (doStrictnessPropagation ps isStrictArg fs)
	// lifting
	# (fs, genfuns) = foldl (upd (isStrictArg ps)) ([], newMap) fs
	# fs = reverse fs ++ elems genfuns

	# (_, world) = writeFile (args!!(2+extra)) (toString (genDefs fs newAppender)) world
	= world 
	
where		
  upd :: (String Int Int -> Bool) ([FuncType], Map String FuncType) FuncType -> ([FuncType], Map String FuncType)
  upd sf (nfs, genfuns) fun 
  	= let (nfun, ngenfuns) = prepareFun sf fun genfuns in ([nfun:nfs], union genfuns ngenfuns)	
  	
  	