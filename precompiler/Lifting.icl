implementation module Lifting

import StdEnv
import Sapl.SaplStruct
import Data.Map

inline :: !SaplTerm -> Bool
inline (SLet _ _) = False
inline (SCase cond [(PLit (LBool True), case1),(PLit (LBool False), case2)]) = inline cond && inline case1 && inline case2
inline (SCase cond [(PLit (LBool False), case1),(PLit (LBool True), case2)]) = inline cond && inline case1 && inline case2
inline (SCase _ _) = False
inline _ = True

prepareFun :: (String Int Int -> Bool) !FuncType (Map String FuncType) -> (FuncType, Map String FuncType)
prepareFun sf (FTFunc name body args) genfuns
	# (body, genfuns) = prepareExpr sf body genfuns 
	= (FTFunc name body args,genfuns)
prepareFun sf (FTCAF name body) genfuns
	# (body, genfuns) = prepareExpr sf body genfuns
	= (FTCAF name body, genfuns)
prepareFun _ ftype genfuns = (ftype, genfuns)

:: LiftingState = {varidx :: Int, genfuns :: Map String FuncType}

genUpdateFun :: SaplTerm -> FuncType
genUpdateFun (SUpdate _ ty updates)
	= FTFunc (TypedVar (NormalVar funName 0) NoType) 
			 (SUpdate (SVar (NormalVar "e" 0)) ty
			 	[(idx, SVar (NormalVar ("a" +++ toString i) 0)) \\ i <- [1..length updates] & idx <- map fst updates])
			 [TypedVar (NormalVar "e" 0) NoType:[TypedVar (NormalVar ("a" +++ toString i) 0) NoType \\ i <- [1..length updates]]]
where
	funName = case ty of
				NoType = "update$" +++ toString (mask updates 0) 
				(Type tn) = "update$" +++ tn +++ "_" +++ toString (mask updates 0) 
				
	mask [] bits = bits	
	mask [(idx,_):us] bits = mask us ((1 << idx) bitor bits)

prepareExpr :: (String Int Int -> Bool) !SaplTerm (Map String FuncType) -> (SaplTerm, Map String FuncType)
prepareExpr sf t genfuns
	# (t, st, defs) = walkTerm t True {varidx = 1, genfuns = genfuns} 
	= case defs of
		[] = (t, st.genfuns)
		defs = (SLet t defs, st.genfuns)
where
	walkTerm :: !SaplTerm !Bool !LiftingState -> (!SaplTerm, !LiftingState, ![SaplLetDef])
	walkTerm c=:(SCase cond patterns) _ st
		# (cond, st, defs) = walkTerm cond True st
		# (patterns, st) = walkPatterns patterns st
		= case defs of
			[] = (SCase cond patterns, st, [])
			defs = (SCase (SLet cond defs) patterns, st, [])

	walkTerm (SLet expr bindings) _ st
		# (expr, st, edefs) = walkTerm expr True st 
		# (bindings, st, bdefs) = walkBindings bindings st 	
		# defs = edefs ++ bdefs
		=  case defs of
			[] = (SLet expr bindings, st, [])  
			// New defs can be added to the end, they will be ordered later on
			defs = (SLet expr (bindings ++ defs), st, [])  

	walkTerm (SSelect expr ty idx) strictPosition st
		# (expr, st, defs) = walkTerm expr strictPosition st 
		=  (SSelect expr ty idx, st, defs)  

	walkTerm (SUpdate expr ty updates) False st
		# (expr, st, edefs) = walkTerm expr False st 
		# (updates, st, udefs) = walkUpdates updates st
		// Generate new fun and lift it in the same time
		# (genfun, _) = prepareFun sf (genUpdateFun (SUpdate expr ty updates)) newMap 
		# funname = extractName genfun
		=  (SApplication (SVar funname) [expr:map snd updates], 
					{st & genfuns = put (unpackVar funname) genfun st.genfuns}, edefs ++ udefs)  
	where
		extractName (FTFunc (TypedVar name _) _ _) = name

	walkTerm (SUpdate expr ty updates) True st
		# (expr, st, edefs) = walkTerm expr True st 
		# (updates, st, udefs) = walkUpdates updates st
		=  (SUpdate expr ty updates, st, edefs ++ udefs) 

	// We always lift this select as strict, does not matter, it is free
	// Arguments are treated as lazy, because we do not know anything about the actual functions
	walkTerm (SApplication sel=:(SSelect sexpr _ _) args) _ st
		# (letvar, st) = genVar st
		# selvar = SVar (removeTypeInfo letvar)
		# (args, st, defs) = walkArgs (repeat False) args st
		= (SApplication selvar args, st, [SaplLetDef letvar sel:defs])
	where
  		genVar {varidx} = (TypedVar (StrictVar ("$g"+++toString varidx) 0) NoType, {st & varidx = varidx + 1})

	walkTerm (SApplication var=:(SVar name) args) True st
		# (args, st, defs) = walkArgs [sf (unpackVar name) (length args) i \\ i <- [0..]] args st
		= (SApplication var args, st, defs)

	walkTerm (SApplication name args) False st
		# (args, st, defs) = walkArgs (repeat False) args st
		= (SApplication name args, st, defs)

	walkTerm t _ st = (t, st, [])

	walkArgs _ [] st = ([], st, [])
	walkArgs [isStrict:si] [t:ts] st
		# (t, st, def) = walkTerm t isStrict st 
		# (ts, st, defs) = walkArgs si ts st
		= ([t:ts], st, def ++ defs)
	
	walkPatterns [] st = ([], st)
	walkPatterns [(p, t):ps] st 
		# (t, st, defs) = walkTerm t True st 
		# t = case defs of
			[]  = t
			defs = SLet t defs 	 
		# (ps, st) = walkPatterns ps st
		= ([(p,t):ps], st)

	walkBindings [] st = ([], st, [])
	walkBindings [SaplLetDef var expr:bs] st 
		# (expr, st, def) = walkTerm expr (isStrictVar var) st
		# (bs, st, defs) = walkBindings bs st
		= ([SaplLetDef var expr:bs], st, def ++ defs)

	walkUpdates [] st = ([], st, [])
	walkUpdates [(idx,expr):us] st 
		# (expr, st, def) = walkTerm expr False st // TODO: check record type
		# (us, st, defs) = walkUpdates us st
		= ([(idx, expr):us], st, def ++ defs)

