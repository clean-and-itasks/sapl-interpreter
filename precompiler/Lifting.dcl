definition module Lifting

import Sapl.SaplStruct
import Data.Map

// Returns True if a term can be inlined, i.e. no separate statement is needed
inline :: !SaplTerm -> Bool

prepareFun :: (String Int Int -> Bool) !FuncType (Map String FuncType) -> (FuncType, Map String FuncType)
prepareExpr :: (String Int Int -> Bool) !SaplTerm (Map String FuncType) -> (SaplTerm, Map String FuncType)



