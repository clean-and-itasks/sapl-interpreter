implementation module Prims

import StdString
import Data.Map

primMap :: Map String Int
primMap =: fromList primList

primList = [
	("addI", 2),
	("subI", 2),
	("multI", 2),
	("divI", 2),
	("gtI", 2),
	("geI", 2),
	("ltI", 2),
	("eqI", 2),
	("neqI", 2),
    
	("bitxor", 2),
	("bitand", 2),
	("bitor", 2),
	("bitnot", 2),
	("shiftleft", 2),
	("shiftright", 2),
    
	("geC", 2),
	("ltC", 2),
	("eqC", 2),

	("addR", 2),
	("subR", 2),
	("multR", 2),
	("divR", 2),
	("ltR", 2),
	("eqR", 2),
	("cos", 1),
	("sin", 1),
	("tan", 1),
	("acos", 1),
	("atan", 1),
	("absR", 1),
	("negR", 1),

	("eqB", 2),
	("not", 1),
	("and", 2),
	("or", 2),
	("mod", 2),

	("eqB", 2),
	("not", 1),
	("and", 2),
	("or", 2),
	("mod", 2),

	("C2I", 1),
	("R2I", 1),
	("S2I", 1),
	("I2C", 1),
	("I2R", 1),
	("R2R", 1),
	("S2R", 1),

	("array_create1", 1),
	("array_create1_lazy", 1),
	("array_create1_B_I", 1),
	("array_create1_B_B", 1),
	("array_create1_B_R", 1),

	("array_create2", 2),
	("array_create2_lazy", 2),
	("array_create2_B_I", 2),
	("array_create2_B_B", 2),
	("array_create2_B_R", 2),
        
	("array_update", 3),
	("array_update_lazy", 3),
	("array_update_B_I", 3),
	("array_update_B_B", 3),
	("array_update_B_R", 3),

	("array_size", 1),
	("array_size_lazy", 1),
	("array_size_B_I", 1),
	("array_size_B_B", 1),
	("array_size_B_R", 1),

	("array_select", 2),
	("array_select_lazy", 2),
	("array_select_B_I", 2),
	("array_select_B_B", 2),
	("array_select_B_R", 2),

	("string_size", 1),
	("string_select", 2),
	("string_create1", 1),
	("string_create2", 2),
	("string_update", 3),
	("string_update_copy", 3),
	("string_slice", 3),
	("string_append", 2),
        
	("eqS", 2),
	("ltS", 2),
	("C2S", 1),
	("I2S", 1),
	("R2S", 1),

	("_trace", 1),
	("abort", 1)]
	
	
