#!/bin/bash

for filenm in $( ls *.sapl); do
  lhs=${filenm%%.*}
  bsapl="$lhs.bsapl"
  out="$lhs.out"
  exp="$lhs.exp"

  if [ -e "$bsapl" ]
  then
    rm -f $bsapl
  fi

  if [ -e "$out" ]
  then
    rm -f $out
  fi

  `pwd`/../precompiler/precompiler.exe $filenm $bsapl
  `pwd`/../interpreter/main $bsapl > $out
  diff $exp $out > /dev/null || echo "$filenm different"
done
