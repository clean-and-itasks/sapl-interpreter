@echo off
for %%F in (*.sapl) do (
	if exist %%~nF.bsapl del %%~nF.bsapl
	if exist %%~nF.out del %%~nF.out
	..\precompiler\precompiler.exe %%~nF.sapl %%~nF.bsapl
	..\interpreter\main.exe %%~nF.bsapl > %%~nF.out
	FC /B %%~nF.exp %%~nF.out > NUL || echo "%%~nF different" 
)