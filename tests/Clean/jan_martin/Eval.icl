module  Eval

import StdEnv

:: Expr = App Expr Expr| Func Int | Var Int | Num Int | Oper (Int->Int->Int) | Relop (Int->Int->Bool) | Boolean Bool | Error

:: MList t1 = Cons t1 (MList t1) | Empty   

ifte b e t | b          = e
           | otherwise  = t
filt p Empty = Empty
filt p (Cons x xs) | p x         = Cons x(filt p xs)
                   | otherwise   = filt p xs

el 0 (Cons x xs) = x 
el n (Cons x xs) = el (n-1) xs
len Empty = 0
len (Cons x xs) = 1 + len xs
drp n Empty      = Empty
drp n (Cons a  as) | n==0      = Cons a  as
                   | otherwise =  (drp (n-1) as)

subst (App l r) es = App (subst l es) (subst r es)
subst (Var n)   es = el n es
subst       x   es = x

eval (App l r) es fs    = eval l (Cons (eval r Empty fs) es) fs
eval (Func f)  es fs  | len es >= fst (el f fs) = eval (subst (snd (el f fs)) es) (drp (fst (el f fs)) es) fs
                      | otherwise               = rebuild (Func f) es
eval (Oper op)    es fs = apply op (el 0 es) (el 1 es)
eval (Relop op)   es fs = eval (apprel op (el 0 es) (el 1 es)) (drp 2 es) fs
eval (Num n)      es fs = Num n
eval (Boolean b)     es fs |len es >= 2 = eval (ifte b (el 0 es) (el 1 es)) (drp 2 es) fs
                           | otherwise  = Boolean b

rebuild e Empty    = e
rebuild e (Cons x xs) = rebuild (App e x) xs

apply op (Num n) (Num m)  = Num (op n m)
apprel op (Num n) (Num m) = Boolean (op n m)

ttttest = eval (App (App (Oper add) (Num 3)) (Num 4)) Empty Empty

add = (+)
mult = (*)
tup a b = (a,b)
eq a b = a == b
sub a b = a - b
md a b = a  rem b

funcs1 = Cons ffac Empty
funcs = (Cons fneq (Cons ftypeof (Cons fnrargs (Cons fnrapps (Cons fstacksize (Cons fError (Cons fsuc (Cons ffac (Cons fEmpty (Cons fCons (Cons ffrom
        (Cons ftake (Cons fdrp (Cons ffilter (Cons fel (Cons fcount (Cons fmember (Cons fnotmodzero (Cons fprimes (Cons fsieve (Cons ftake0 (Cons fdrp0
        (Cons ffilter0 (Cons fel0 (Cons fcount0 (Cons fmember0 (Cons fsieve0 Empty)))))))))))))))))))))))))))
fneq = (tup 2 (Error))
ftypeof = (tup 1 (Error))
fnrargs = (tup 1 (Error))
fnrapps = (tup 1 (Error))
fstacksize = (tup 1 (Error))
fError = (tup 1 ((Var 0)))
fsuc = (tup 1 ((App (App (Oper add) (Var 0)) (Num 1))))
ffac = (tup 1 ((App (App (App (App (Relop eq) (Var 0)) (Num 0)) (Num 1)) (App (App (Oper mult) (Var 0)) (App (Func 7) (App (App (Oper sub) (Var 0)) (Num 1)))))))
fEmpty = (tup 2 ((Var 0)))
fCons = (tup 4 ((App (App (Var 3) (Var 0)) (Var 1))))
ffrom = (tup 1 ((App (App (Func 9) (Var 0)) (App (Func 10) (App (Func 6) (Var 0))))))
ftake = (tup 2 ((App (App (Var 1) (Func 8)) (App (Func 20) (Var 0)))))
fdrp = (tup 2 ((App (App (Var 1) (Func 8)) (App (Func 21) (Var 0)))))
ffilter = (tup 2 ((App (App (Var 1) (Func 8)) (App (Func 22) (Var 0)))))
fel = (tup 2 ((App (App (Var 1) (Func 5)) (App (Func 23) (Var 0)))))
fcount = (tup 1 ((App (App (Var 0) (Num 0)) (Func 24))))
fmember = (tup 2 ((App (App (Var 0) (Boolean False)) (App (Func 25) (Var 1)))))
fnotmodzero = (tup 2 ((App (App (App (App (Relop eq) (App (App (Oper md) (Var 1)) (Var 0))) (Num 0)) (Boolean False)) (Boolean True))))
fprimes = (tup 0 ((App (Func 19) (App (Func 10) (Num 2)))))
fsieve = (tup 1 ((App (App (Var 0) (Func 5)) (Func 26))))
smain a = eval ((App (App (Func 14) (Num a)) (Func 18))) Empty funcs
ftake0 = (tup 3 ((App (App (App (App (Relop eq) (Var 0)) (Num 0)) (Func 8)) (App (App (Func 9) (Var 1)) (App (App (Func 11) (App (App (Oper sub) (Var 0)) (Num 1))) (Var 2))))))
fdrp0 = (tup 3 ((App (App (App (App (Relop eq) (Var 0)) (Num 0)) (App (App (Func 9) (Var 1)) (Var 2))) (App (App (Func 12) (App (App (Oper sub) (Var 0)) (Num 1))) (Var 2)))))
ffilter0 = (tup 3 ((App (App (App (Var 0) (Var 1)) (App (App (Func 9) (Var 1)) (App (App (Func 13) (Var 0)) (Var 2)))) (App (App (Func 13) (Var 0)) (Var 2)))))
fel0 = (tup 3 ((App (App (App (App (Relop eq) (Var 0)) (Num 0)) (Var 1)) (App (App (Func 14) (App (App (Oper sub) (Var 0)) (Num 1))) (Var 2)))))
fcount0 = (tup 2 ((App (Func 6) (App (Func 15) (Var 1)))))
fmember0 = (tup 3 ((App (App (App (App (Relop eq) (Var 1)) (Var 0)) (Boolean True)) (App (App (Func 16) (Var 2)) (Var 0)))))
fsieve0 = (tup 2 ((App (App (Func 9) (Var 0)) (App (Func 19) (App (App (Func 13) (App (Func 17) (Var 0))) (Var 1))))))

test n |(n >= 0)     = (test (n - 1)) 
       | otherwise   = 1111


domain m = getNum (smain m)
getNum (Num n) = n

Start =  (domain 100)
 