module Sprimes

import StdEnv

:: Natn = Zero | Suc Natn 
:: MList t1 = Cons t1 (MList t1) | Empty

fil p Empty = Empty
fil p (Cons a as) | p a           = Cons a (fil p as) 
                  | otherwise     = fil p as

ifte b e t | b          = e
           | otherwise  = t

el Zero (Cons a  as) = a
el (Suc n) (Cons a as) = el n as

fr n = Cons n (fr (Suc n))

add Zero m = m
add (Suc n) m = Suc (add n m)
prd (Suc n) = n

sub n Zero = n
sub n (Suc m) = prd (sub n m)

gt Zero n =  False
gt (Suc n) Zero = True
gt (Suc n) (Suc m) = gt n m

eq Zero Zero = True
eq (Suc n) (Suc m) = eq n m
eq n       m       = False

mmod n m  = ifte (gt m n) n (mmod (sub n m) m)

two = Suc (Suc Zero)

notmodzero x y = ifte (eq (mmod y x) Zero) False True
primes = sieve (fr two)
sieve (Cons x xs) = Cons x (sieve (fil (notmodzero x) xs))

five = Suc (Suc (Suc (Suc (Suc Zero))))
ten = add five five
twenty = add ten ten
forty = add twenty twenty
eighty = add forty forty
h60 = add eighty eighty
h200 = add h60 forty
h240 = add h200 forty
h280 = add h200 eighty

mkeNum Zero = 0
mkeNum (Suc n) = 1+mkeNum n

domain n = mkeNum (el n primes) 

Start =  (domain h280) 
