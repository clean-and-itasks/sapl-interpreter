module Parselib

import StdEnv

:: List1 t = Nill | Cons t (List1 t)
:: Tuptype a b = Tup a b
:: Tree t = Bin t (Tree t) (Tree t) | Empty

length1 Nill = 0
length1 (Cons x xs) = 1 + length1 xs

take1 n Nill         = Nill
take1 n (Cons x xs) |n==0 = Nill
                    | otherwise  = Cons x (take1 (n-1) xs)

drop1 n Nill         = Nill
drop1 n (Cons x xs) | n==0 = Cons x xs
                    | otherwise = drop1 (n-1) xs

fr n = Cons n (fr (n+1))
fromto n m = take1 (m-n+1) (fr n)


filter1 f Nill     = Nill
filter1 f (Cons x xs) | f x       = Cons x (filter1 f xs)
                      | otherwise = filter1 f xs
fstp (Tup f s) = f

conc Nill = Nill
conc (Cons Nill yss) = conc yss
conc (Cons (Cons z zs) yss)  = Cons z (conc (Cons zs yss))

:: JustType a = Just a | Nothing


hd1 (Cons x xs) = x



append1 Nill         ys = ys
append1 (Cons x xs) ys = Cons x (append1 xs ys)

map1 f Nill         = Nill
map1 f (Cons x xs) = Cons (f x) (map1 f xs)


concat1 Nill           = Nill
concat1 (Cons Nill xss)     = concat1 xss
concat1 (Cons (Cons x xs) xss) = Cons x (concat1 (Cons xs xss))

foldlp f z Nill = z
foldlp f z (Cons x xs) = foldlp f (f z x) xs

foldrp f z Nill = z
foldrp f z (Cons x xs) = f x (foldrp f z xs)

zipp d Nill = Nill
zipp Nill e = Nill
zipp (Cons x xs) (Cons y ys) = Cons (Tup x y)  (zipp xs ys)

eqlist Nill         Nill         = True
eqlist (Cons x xs) (Cons y ys) =  (eqChar x  y) && (eqlist xs ys)
eqlist as          bs          = False

sum1 Nill         = 0
sum1 (Cons x xs) = x + sum1 xs

eqChar a b = a == b
//flip f a b = f b a

// Elementary parsers

pSymbol a Nill    =  Nill
pSymbol a (Cons x xs) = if  (eqChar x  a)   (Cons (Tup x xs) Nill)  Nill

pSatisfy p Nill     =  Nill
pSatisfy p (Cons x xs) = if  (p x)   (Cons (Tup x xs) Nill)  Nill

pToken k xs = if (eqlist k (take1 (length1 k) xs))  (Cons (Tup k (drop1 (length1 k) xs)) Nill)  Nill

pFailp xs  =  Nill

pSucceed r xs  =  Cons (Tup r xs) Nill

// Parser combinators

choice p  q xs  =  append1 (p xs) (q xs)

sequ p  q xs  =  concat1(map1 (do2 q) (p xs))
do1 f (Tup x zs) = Tup (f x) zs
do2 q (Tup f ys)  = map1 (do1 f) (q ys)

seqapply f p = sequ (pSucceed f) p


option p d  =  choice p (pSucceed d)

// Common situations

//const f x = f

// p <* q    = Const <$> p <*> q
// p *> q    = flip Const <$> p <*> q
// f <$ p    = Const f <$> p

seqleft p  q      = sequ (seqapply const  p) q
seqright p  q     = sequ (seqapply (flip const) p)  q
seqapplyleft f  p = seqapply (const f) p

//pMany p  =  choice (seqf (sequ list  p)  (pMany p)) (pSucceed Nill)
pMany p  =  option (sequ (seqapply list  p)  (pMany p)) Nill
pMany1 p  = sequ (seqapply list p) (pMany p)
pPack p r q  =  seqleft (seqright p  r)  q
pListSep p s  =  sequ (seqapply list  p) (pMany (seqright s  p))

determ p xs         = dt (p xs)
dt Nill         = Nill
dt (Cons r xs) = Cons r Nill

greedy   p =  determ (pMany p)
greedy1  p =  determ (pMany1 p)

pDigit  =  pSatisfy isDigit
//isDigit c = (c >='0') && ( '9' >= c)
//isAlpha c = ( (c >= 'a')&& ( 'z' >= c)) || (( c >= 'A') &&( 'Z' >=c))
isAlphaNum c =  (isAlpha c) ||(isDigit c)
pDigAsInt  =  seqapply dig2num  pDigit
dig2num c = fromChar c - fromChar '0'

pNatural  =  seqapply (foldlp makeNum 0) (pMany1 pDigAsInt)
makeNum a b = a*10 + b
list x xs = Cons x xs

//pInteger  =  (negate <$ (pSymbol '-') `option` id ) <*>  pNatural
//pInteger  =  sequ (option (seqapplyleft negate  (pSymbol '-'))  id)   pNatural
pNeg  =  sequ (seqapplyleft negate  (pSymbol '-'))   pNatural
pNeg2 = seqapply negate (seqright (pSymbol '-') pNatural)

negate a = 0-a
//id x = x

pIdentifier =  sequ ((seqapply list)  (pSatisfy isAlpha)) (greedy (pSatisfy isAlphaNum))
pParens p  =  pPack (pSymbol '(') p (pSymbol ')')
pCommaList p  =  pListSep p (pSymbol ',')

pSequence Nill      =  pSucceed Nill
pSequence (Cons p ps)  =  sequ (seqapply list  p)  (pSequence ps)

//lchoice  =  foldrp option pFailp


//numlist = pPack (pSymbol '[') (pCommaList pInteger) (pSymbol ']')
//numidlist = pPack (pSymbol '[') (pCommaList (choice pInteger pIdentifier)) (pSymbol ']')

getres f = fstp (hd1 f)

// parser for prolog rules
:: Term  =  Con Int|  Var Int |  Fun (List1 Char) (List1 Term)
:: Rulet = Rule Term (List1 Term)

con n = Con n
var n = Var n
fun n ns = Fun n ns
rule r rs = Rule r rs

pComma = pSymbol ','
//isSpace x =  (x == ' ')|| (x == '\n')

pCon   = seqapply con pNatural
pVar   =  seqapply var (seqright (pSymbol 'X') pNatural)
pFun   = sequ (seqapply fun  pIdentifier) (choice   (pParens (pListSep pTerm pComma)) (pSucceed Nill))
pTerm  = choice (choice pCon  pVar)  pFun
pRule  = seqleft (sequ (seqapply rule  pTerm) (option pRightHandSide  Nill)) (pSymbol '.')
pRightHandSide =     seqright (seqright (pSymbol ':') (pSymbol '-')) (pListSep pTerm pComma)


start p xs = fstp ( (hd1  (p  (filter1 ( not o isSpace) xs))))
parseTerm  = start pTerm
parseRules = start (pMany pRule)
parseRule  = start pRule

//not x = ~x

// Prolog interpreter

emptyEnv   = Just Nill

//tag :: num -> term -> term
tag n (Con x)     = Con x
tag n (Var x)     = Var (n + 10 * x)
tag n (Fun x xs)  = Fun x (map1 (tag n) xs)

//tagRule :: num -> rule -> rule
tagRule n (Rule c cs) = Rule (tag n c) (map1 (tag n) cs)

//bind :: num -> term -> bdgs -> bdgs
bind v t bs = Cons (Tup v t)  bs

// lookUp :: bdgs -> term -> term
lookUp bs (Var x)     = look bs x
lookUp bs  t          = t
//
// look :: bdgs -> num -> term
look Nill         x         = Var x
look (Cons (Tup z t) bs) x = if (x == z)  (lookUp bs t)  (look bs x)

//unify ::  (term , term) -> env -> env
unify  d      Nothing      = Nothing
unify  (Tup t u) (Just bs) = uni (lookUp bs t) (lookUp bs u) bs

uni (Con x)    (Con y)    bs = if ( x <> y)  Nothing  (Just bs)
uni (Fun x xs) (Fun y ys) bs = if ( (eqlist x  y) && (length1 xs == length1 ys))  (foldrp unify (Just bs) (zipp xs ys))   Nothing
uni (Var x)    p1         bs = Just (bind x p1 bs)
uni p0         (Var y)    bs = Just (bind y p0 bs)
uni p0         p1         bs = Nothing

//solve :: [rule] -> [term] -> env -> num -> [env]
solve rules Nill    e d = Cons e Nill
solve rules (Cons t ts) e n =  concat1 (map1 ((f e t rules ts n) o (tagRule n)) rules)
f e t rules ts n (Rule c cs) = fh rules ts n cs (unify (Tup t c) e)
fh rules ts n cs Nothing = Nill
fh rules ts n cs newenv  =  solve rules (append1 cs ts) newenv (n+1)

dosol inp = solve (parseRules prog50) (Cons (parseTerm (list2List1 inp)) Nill) emptyEnv  0
//prolog inp = solve (parseRules (ffread ['voorbeeld.prl[')) (Cons (parseTerm (list2List1 inp)) Nill) emptyEnv  0

isSingle (Rule (Fun f fs) cs) = (length1 fs == 1)&& (length1 cs == 0)

parsetest n = if (n==0)  Nill  (Cons (length1 (filter1 isSingle (parseRules prog100))) (parsetest (n-1)))
//parsetest n = if (n=0) Nill (Cons (length1 (filter1 isSingle (parseRules (ffread ['large.prl[')))) (parsetest (n-1)))

//ffread fn = list2List1 (fread fn)
list2List1 [] = Nill
list2List1 [x:xs] = Cons x (list2List1 xs)

sol inp  n =  length1 (solve (parseRules prog) (Cons (parseTerm  inp) Nill) emptyEnv  0)
msol n = sum1 (map1 (sol (list2List1 ['voorouder(X1,marit)'])) (fromto 1 n))

prologtest = length1 (dosol ['vader(X1,mark)'])
prog = concat1 (list2List1 (map list2List1 pprog))
pprog = [line1,line2,line3,line4,line5,line6,line7,line8,line9,line10,line11,line12,line13,line14,line15,line16,line17]
prog10 = concat1 (Cons prog (Cons prog (Cons prog (Cons prog (Cons prog (Cons prog (Cons prog (Cons prog (Cons prog (Cons prog Nill))))))))))
prog20 = concat1 (Cons prog10 (Cons prog10  Nill))
prog40 = concat1 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10  Nill))))
prog50 = concat1 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10  Nill)))))
prog100 = concat1 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 (Cons prog10 Nill))))))))))
line1 = ['man(jan).']
line2 = ['man(mark).']
line3 = ['vrouw(margriet).']
line4 = ['vrouw(marit).']
line5 = ['ouder(jan,mark).']
line6 = ['ouder(jan,marit).']
line7 = ['ouder(margriet,marit).']
line8 = ['ouder(margriet,mark).']
line9 = ['moeder(X1,X2) :- ouder(X1,X2),vrouw(X1).']
line10 = ['vader(X1,X2) :- ouder(X1,X2),man(X1).']
line11 = ['ouder(joop,jan).']
line12 = ['ouder(nel,jan).']
line13 = ['grootouder(X1,X2) :- ouder(X1,X3),ouder(X3,X2).']
line14 = ['voorouder(X1,X2) :- ouder(X1,X2).']
line15 = ['voorouder(X1,X2) :- ouder(X1,X3),voorouder(X3,X2).']
line16 = ['ouder(siem,nel).']
line17 = ['ouder(cornelia,nel).']

//Start = parsetest 10
Start = msol 500