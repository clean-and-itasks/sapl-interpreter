module sort

import StdEnv

:: List1 t = Nill | Cons t (List1 t)

:: Tree t = Bin t (Tree t) (Tree t) | Empty


length1 Nill = 0
length1 (Cons x xs) = 1 + length1 xs

fr n = Cons n (fr (n+1))
fromto n m = take1 (m-n+1) (fr n)

//sum1 :: List Int -> Int
sum1 Nill         = 0
sum1 (Cons x xs) = x + sum1 xs

//take1 :: Int -> List t -> List t
take1 n Nill         = Nill
take1 n (Cons x xs) |n==0 = Nill
                    | otherwise  = Cons x (take1 (n-1) xs)

drop1 n Nill         = Nill
drop1 n (Cons x xs) | n==0 = Cons x xs
                    | otherwise = drop1 (n-1) xs

append1 Nill ys =  ys
append1 (Cons x xs) ys = Cons x (append1 xs ys)

filter1 f Nill     = Nill
filter1 f (Cons x xs) | f x       = Cons x (filter1 f xs)
                      | otherwise = filter1 f xs


// treesort
//listtotree :: List Int -> Tree Int
listtotree Nill = Empty
listtotree (Cons x xs) = instree x (listtotree xs)

treetolist Empty       = Nill
treetolist (Bin x l r) = append1 (treetolist l) (Cons x (treetolist r))

treetest n = sum1 (treetolist (listtotree (fromto 1 n)))

//insert :: Int -> Tree Int -> Tree Int
instree a Empty = Bin a Empty Empty
instree a (Bin b l r) = if (b >= a)  (Bin b (instree a l) r)  (Bin b l (instree a r))


//mklist :: Int -> List Int
mklist n = mkl 0 n 0
mkl m n k = if (k==10)  Nill  (mklaf m n k)
mklaf m n k = if (n > m) (Cons (m*10 + k) (mkl (m+1) n k)) (mkl 0 n (k+1))

treesort xs =  treetolist (listtotree xs)
treesorttest n =  treesort (mklist n)

// qsort
smaller x xs =  filter1 (\y.y < x) xs
larger  x xs = filter1 (\y.y >= x) xs
qsort Nill   = Nill
qsort (Cons x xs) = append1 (qsort (smaller x xs)) (Cons x (qsort (larger x xs)))
qsorttest n = qsort (mklist n)

// isort
ins x Nill = Cons x Nill
ins x (Cons y ys) | x <= y    = Cons x (Cons y ys)
                  | otherwise = Cons y (ins x ys)
isort Nill         = Nill
isort (Cons x xs)  = ins x (isort xs)
isorttest n = isort (mklist n)

// msort
merge1 Nill         ys          = ys
merge1 xs           Nill        = xs
merge1 (Cons a as) (Cons b bs) | a == b    = Cons a (merge1 as bs)
                               | a < b     = Cons a (merge1 as (Cons b bs))
                               | otherwise = Cons b (merge1 (Cons a as) bs)

mergesort Nill          = Nill
mergesort (Cons x Nill) = Cons x Nill
mergesort (Cons x xs)   = let n = (length1 (Cons x xs))/ 2 in merge1 (mergesort (take1 n (Cons x xs))) (mergesort (drop1 n (Cons x xs)))
msorttest n = mergesort (mklist n)

Start = allsort

allsort = Cons (sum1 (treesorttest 600)) (Cons (sum1 (qsorttest 600)) (Cons (sum1 (isorttest 600)) (Cons (sum1 (msorttest 4000)) Nill)))

