module Benchmarks

import StdEnv

:: List1 t = Nill | Cons t (List1 t)

:: Tree t = Bin t (Tree t) (Tree t) | Empty



cm Nill          = 0
cm (Cons a Nill) = a
cm (Cons a (Cons b Nill)) = a+b
cm (Cons a (Cons b (Cons c Nill))) = a+b+c
cm (Cons a (Cons b (Cons c (Cons d Nill)))) = a+b+c+d
cm (Cons a (Cons b (Cons c (Cons d xs)))) = a+b+c+d + length1 xs

length1 Nill = 0
length1 (Cons x xs) = 1 + length1 xs

mtest n = sum1 (map1 (\m -> cm (fromto 1 ( m  rem 10))) (fromto 1 n))
matchtest n = sum1 (map1 mtest (fromto 1 n))

take1 n Nill         = Nill
take1 n (Cons x xs) |n==0 = Nill
                    | otherwise  = Cons x (take1 (n-1) xs)

drop1 n Nill         = Nill
drop1 n (Cons x xs) | n==0 = Cons x xs
                    | otherwise = drop1 (n-1) xs

fr n = Cons n (fr (n+1))
fromto n m = take1 (m-n+1) (fr n)

append1 Nill         ys = ys
append1 (Cons x xs) ys = Cons x (append1 xs ys)

map1 f Nill         = Nill
map1 f (Cons x xs) = Cons (f x) (map1 f xs)

filter1 f Nill     = Nill
filter1 f (Cons x xs) | f x       = Cons x (filter1 f xs)
                      | otherwise = filter1 f xs

sum1 Nill         = 0
sum1 (Cons x xs) = x + sum1 xs

listtotree Nill               = Empty
listtotree (Cons x Nill)      = Bin x Empty Empty
listtotree (Cons x xs)       = Bin x (listtotree (take1 ((length1 xs) / 2) xs)) (listtotree (drop1 ((length1 xs) / 2) xs))

treetolist Empty       = Nill
treetolist (Bin x l r) = Cons x (append1 (treetolist l) (treetolist r))

sumtree Empty       = 0
sumtree (Bin x l r) =  x + sumtree l + sumtree r

depthtree Empty       = 0
depthtree (Bin x l r) =  1 + max2 (depthtree l) (depthtree r)

ltl xs = treetolist (listtotree xs)

treetest n = sum1 (treetolist (listtotree (fromto 1 n)))
treetest1 n = sumtree (listtotree (fromto 1 n))
treetest2 n = depthtree (listtotree (fromto 1 n))

rep1 n a | n==0 = Nill
         | otherwise = (Cons a (rep1 (n-1) a))

reptreetest n m = sum1 (map1 (\d -> (treetest m) rem d)  (fromto 2 n))

el1 n (Cons a as) | n==0       = a
                  | otherwise  = el1 (n-1) as

merge1 Nill         ys          = ys
merge1 xs           Nill        = xs
merge1 (Cons a as) (Cons b bs) | a == b    = Cons a (merge1 as bs)
                               | a < b     = Cons a (merge1 as (Cons b bs))
                               | otherwise = Cons b (merge1 (Cons a as) bs)

hamming k = let h =  Cons (f k) (merge1 (merge1 (map1 (mult 2) h) (map1 (mult 3) h)) (map1 (mult 5) h)) in h

f k | k rem 2 == 0 = 1
    | otherwise    = k rem 2

mult a b = a * b

max2 x y | x > y = x
         | otherwise = y
 
frominc n i = Cons n (frominc (n+i) i)
mergetest  = merge1 (frominc 2 2) (frominc 0 3) 

//rephamming n  = sum1 (map1 hm (fromto 1 n))
rephamming n m = sum1 (map1 hm (rep1 n m))
hm n =  (el1 n (hamming n)) rem  999

tt k= let a = Cons k (map1 (\b -> b+1) a) in a

sumrem Nill = 0
sumrem (Cons a as) = (a +(sumrem as)) rem 7 

test n = sum1 (map1 (el1 n) (rep1 n (tt 10)))

fib n | n < 2 = 1
      | otherwise = fib (n-2) + fib (n-1)

//twice f x = f (f x) 
//inc a =  a + 1
cons x xs = Cons x xs
tinc n = twice twice twice twice inc n
reptwice n = sumrem (map1 tinc (fromto 1 n))        

smaller x xs =  filter1 (\y.y < x) xs
larger  x xs = filter1 (\y.y >= x) xs
qsort Nill   = Nill
qsort (Cons x xs) = append1 (qsort (smaller x xs)) (Cons x (qsort (larger x xs)))
downto n m | m > n = Nill
           | otherwise = Cons n (downto (n-1) m)
sorttest n = sum1 (qsort (mklist n))
sorttestu n = sum1 (qsort (fromto 1 n))
sorttestd n = sum1 (qsort (downto n 1))
repsort n = sum1 (map1 sorttest (fromto 1000 (1000 + n)))

ins x Nill = Cons x Nill
ins x (Cons y ys) | x <= y    = Cons x (Cons y ys)
                  | otherwise = Cons y (ins x ys)
isort Nill         = Nill
isort (Cons x xs)  = ins x (isort xs)

mergesort Nill          = Nill
mergesort (Cons x Nill) = Cons x Nill
mergesort (Cons x xs)   = let n = (length1 (Cons x xs))/ 2 in merge1 (mergesort (take1 n (Cons x xs))) (mergesort (drop1 n (Cons x xs)))

fromdown n m = fd n m 0
fd n m k | k == m      =  [] 
         | otherwise   = [n+k:[m-k:fd n m (k+1)]] 

quicksort [] = []
quicksort [x:xs] = quicksort (filter (\y. y < x) xs) ++ [x: quicksort (filter (\y. y >= x) xs)]
qsorttest n = sum (quicksort (fromdown 1 n))

mklist n = mkl 0 n 0
mkl _ _ 10 = Nill
mkl m n k  = if (m < n) (Cons (m*10+k) (mkl (m+1) n k)) (mkl 0 n (k+1))

queens n = length (qqueens n n)
qqueens k 0 = [[]]
//qqueens k n= [x:xs| xs <- qqueens k (n-1), x <- [1..k], safe xs 1 x]
qqueens k n = flatten (map (recqq k) (qqueens k (n-1)))
recqq k xs = mapfil (safe xs 1) (rcons xs) [1..k]

mapfil p f []     = []
mapfil p f [x:xs] | p x       = [f x : mapfil p f xs]
                  | otherwise = mapfil p f xs

safe [] d x     = True
safe [y:ys] d x = x <> y && x+d <> y && x-d <> y && safe ys (d+1) x
rcons xs x = [x:xs]

:: Tuptype  a b = Tup a b

knights1 n = doknights1 n (Cons (Tup 1 1) Nill)

doknights1 n (Cons p ps)  | length1 ps+1  == n*n = Cons (Cons p ps) Nill
                          | otherwise            = conc (map1 (\q -> doknights1 n (Cons q (Cons  p ps)))(moves1 n ps p))


moves1 n as (Tup p q) = filter1 (legal1 n as) (Cons (Tup (p+1) (q+2)) (Cons (Tup (p+1)(q-2))(Cons (Tup (p-1)(q+2))(Cons (Tup (p-1)(q-2))
                                        (Cons (Tup (p+2)(q+1))(Cons (Tup (p+2)(q-1))(Cons (Tup (p-2)(q+1))(Cons (Tup (p-2)(q-1)) Nill))))))))

legal1 n as (Tup p q) = 1 <= p && p <= n && 1 <= q && q <= n && (notmemb as (Tup p q))

notmemb Nill a = True
notmemb (Cons x xs) a = neqtup a x && notmemb xs a

neqtup (Tup a b) (Tup c d) = a<>c || b<>d

conc Nill = Nill
conc (Cons Nill yss) = conc yss
conc (Cons (Cons z zs) yss)  = Cons z (conc (Cons zs yss))

hd1 (Cons x xs) = x

//Start =  reptreetest 100 10000
//Start =  (matchtest 2000)
//Start =  (el1 10000000 mergetest)   
//Start =  (fib 35)
//Start =  (rephamming 10000 1000) 
//Start =  (rephamming 4000 1000) 
Start = reptwice 400
//Start = sorttestd 5000
//Start = queens 11
//Start =  sumrem (isort (mklist 1500))
//Start =  sumrem (mergesort (mklist 25000))
//Start = hd1 (knights1 5)

