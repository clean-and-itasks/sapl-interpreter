module primes

import StdEnv
//import	ostime, ossystem, /*MW11++*/ ostick

//Start :: !*World -> (!Tick,	!*World)
/*Start world 
 #  (t1,world) = os_getcurrenttick world
    p          = el 5000 pr
    (t2,world) = os_getcurrenttick world
= (t1,t2,p)
*/

Start = el 5000 pr 

primes = sieve [2..]

sieve [x:xs] = [x : sieve [y\\ y <- xs| y rem x <> 0]]

:: Mylist t1 = Cons t1 (Mylist t1) | Empty 

filt p Empty = Empty
filt p (Cons x xs) | p x = Cons x (filt p xs)
                         = filt p xs

fr n = Cons n (fr (n+1))

el 0 (Cons x xs) = x
el n (Cons x xs) = el (n-1) xs

s (Cons x xs) = Cons x (s (filt (nmz x) xs))

pr = s (fr 2)                          
nmz x y = y rem x <>  0

                         
cons x xs f g = g x xs
nil       f g = g

//len xs = xs 0 len1 
//len1 x xs = 1 + len xs
