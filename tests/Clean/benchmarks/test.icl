module test

import StdEnv
import R 
 
:: R = {r1 :: Int, r2 :: !String, r3 :: Int}
 
a :: {#Char}
a = {'1','2','3'}

b :: {#Int}
b = {1,2,3}

c :: {#Bool}
c = {True,False,True}

d :: {#Real}
d = {1.1, 2.2, 3.3}

e :: {!Int}
e = {1, 2, 3}

f :: {Int}
f = {1, 2, 3}

t a0 a1 a2 a3 a4 a5 a6 a7 a8 a9 b0 b1 b2 b3 b4 b5 b6 b7 b8 b9 c0 c1 c2 c3 a0f a1f a2f a3f a4f a5f a6f a7f a8f a9f b0f b1f b2f b3f b4f b5f b6f b7f b8f b9f c0f c1f cf2 cf3 = a0

r a = {r1 = a, r2 = "buu", r3 = a + 1}
//Start = (r 3).r2

ff r = r.r2
fff b r = if b r.r2 "nee"

fs a = "muu"
fi a = 56

Start 
	# g1 = r 3
	= g1.r2 +++ ff {g1 & r2 = fs 1, r1 = fi 1} 
	//= { g & r2 = fs 1, r1 = fi 1}