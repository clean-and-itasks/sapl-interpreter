module real2

import StdEnv

to2dec :: !Real -> Real
to2dec n = toReal (toInt (n * 100.0)) / 100.0

Start = toString (123.456)
