module array_create

import StdEnv

a :: {#Char}
a = {'1','2','3'}

b :: {#Int}
b = {1,2,3}

c :: {#Bool}
c = {True,False,True}

d :: {#Real}
d = {1.1, 2.2, 3.3}

e :: {!Int}
e = {1, 2, 3}

f :: {Int}
f = {1, 2, 3}

Start = (a, b, c, d, e, f)