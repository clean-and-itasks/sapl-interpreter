module record

import StdEnv

:: R = {f1 :: Int, f2 :: String}

sel1 {f1} = f1
sel2 {f2} = f2

rec = {f1 = 9, f2 = "nine"}

//Start = (sel1 rec, sel2 rec)
Start
	# rec = {rec & f1 = 11, f2 ="eleven"} 
	= (rec.f1, rec.f2)