module os

import StdEnv

/*
f a b = a + b
g c = f c
h f = f 1 2
Start = h g
*/

/*
f a b = a + b
g c = f c
i = g
h f = f 1 2
Start = h i
*/

:: List x = Cons x (List x) | Nil

comp x y = y
//comp x = id

foldr f z Nil = z
foldr f z (Cons x xs) = f x (foldr f z xs)

uniq xs = foldr comp Nil xs

Start = uniq (Cons 1 (Cons 1 Nil))
