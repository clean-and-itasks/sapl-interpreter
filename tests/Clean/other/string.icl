module string

import StdEnv

str = "Hello World!"

Start = "123" < "12"

//Start = str
//Start = size str
//Start = str.[4]
//Start = (str := (0,'h')) := (6, 'w')
//Start = str % (6,10)
//Start = "Hello " +++ str % (6,10) +++ "!"

//Start = (if (str == "") "A" "B", if (str == "Hello" +++ " " +++ "World!") "A" "B")
/*
Start = case str of
          "" = "A"
          "Hello World!" = "B"
*/          
