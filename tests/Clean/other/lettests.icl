module lettests

import StdEnv

test1 = let y = 1+2 
            z = 4+6 in y+z

test2 x y = let r = 3
                s = 6
                in r*x + s*y 

test3 x y = let r = 3*x
                s = 6*y
                in r + s

test4 x y = let r = 3 * x
                s = 6 * y * r
                t = 2 * s
                in r + s + t

test5 = let x = [1:x] in x
test6 = let x = [1:y]
			y = [2:x] in x

//Start = test2 2 4
//Start = test3 2 4
//Start = test4 2 4
Start = take 5 test6
