module trace

import StdEnv, StdDebug

emitStr []         k  =  k
emitStr [c:cs] k  =  emit c (emitStr cs k)

emit :: !Int a -> a
emit c k = trace (toChar c) k

emitInt :: !Int a -> a
emitInt i k = trace i k

char :: !Char -> Int
char c = fromChar c 

str :: !String -> [Int]
str "" = []
str s = [char (select s 0):(str (s % (1,size s)))]

Start = emitStr (str "Success") 5
