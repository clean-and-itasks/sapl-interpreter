module sapl2js

import StdEnv
import Sapl.Target.JS.CodeGeneratorJS, Sapl.Target.Flavour
import Text, System.GetOpt, System.CommandLine, System.Environment, System.File, System.FilePath, Text.JSON
from Sapl.FastString import charIndex

print str world 
	# (con, world) = stdio world
	# con = fwrites str con
	= snd (fclose con world)

:: CmdArgs = Version | Help | Trampoline | Output String | Flavour String

instance == CmdArgs
where
 (==) Version Version = True
 (==) Help Help = True 
 (==) Trampoline Trampoline = True
 (==) (Output a) (Output b) = a == b
 (==) _ _ = False

options :: [OptDescr CmdArgs]
options =
   [Option ['v']     ["version"]      (NoArg Version)            "show version info",
    Option ['h','?'] ["help"]         (NoArg Help)               "show usage info",   
    Option ['f']     ["flavour"]      (ReqArg Flavour "FLAVOUR") "use FLAVOUR for generating JavaScript (default \"clean\")",
    Option ['o']     ["output"]       (ReqArg Output "FILE")     "use FILE for output",    
    Option ['t']     ["trampoline"]   (NoArg Trampoline)         "enable trampoline optimization"]

header = "Usage: sapl2js [OPTION...] file"
error es world = print (concat es +++ "\n" +++ usageInfo header options +++ "\n") world
ioerror msg world = print msg world

getOutput inp [] = dropExtension inp +++ ".js"
getOutput inp [Output o:os] = o
getOutput inp [_:os] = getOutput inp os

getFlavour [] = "clean"
getFlavour [Flavour f:os] = f
getFlavour [_:os] = getFlavour os

main bindir inp os world 

	# (mbFlavfile, world) = findflav [".", bindir, bindir </> "../share/sapl/", bindir </> "../share/"] world
	| isNothing mbFlavfile
		= ioerror ("Flavour file " +++ flavfile +++ " cannot be found") world

	# (flavres, world) = readFile (fromJust mbFlavfile) world
	| isError flavres
		= ioerror ("An error occured while reading file " +++ flavfile +++ ": " +++ toString (fromError flavres)) world

	# mbFlav = toFlavour (fromOk flavres)
	| isNothing mbFlav
		= ioerror ("Error in file " +++ flavfile) world

	# (inpres, world) = readFile inp world
	| isError inpres
		= ioerror ("An error occured while reading file " +++ inp +++ ": " +++ toString (fromError inpres)) world
		
	# pts = tokensWithPositions (fromOk inpres)
	= case generateJS (fromJust mbFlav) trampoline (fromOk inpres) Nothing of
			Error msg = ioerror ("An error occured while generating javaScript: " +++ msg) world
			Ok (a, _) 
				# (res, world) = withFile output FWriteData (intoFile a) world
				| isError res
					= ioerror ("An error occured while writing file " +++ inp +++ ": " +++ toString (fromError res)) world
					= world
					
where
	trampoline = isMember Trampoline os	
	output = getOutput inp os
	
	flavfile = getFlavour os +++ ".f" 
	findflav [] world = (Nothing, world)
	findflav [d:ds] world
		= case fileExists (d </> flavfile) world of
			(True, world) = (Just (d </> flavfile), world)
			(_   , world) = findflav ds world 			

Start world
	# (cmdline, world) = getCommandLine world
	# (os, n, errs) = getOpt Permute options (drop 1 cmdline)
	
	| isMember Version os
		= print "sapl2js: Sapl to JavaScript compiler, version 2013-11-08\n" world
	
	| isMember Help os
		= print (usageInfo header options +++ "\n") world
	
	# (bindir, world) = case getEnvironmentVariable "SAPL_HOME" world of
				(Just dir, world) = (dir </> "bin", world)
				(Nothing , world) = (takeDirectory (hd cmdline), world)	
	
//	= case errs of
	//	[]   -> case n of
//					[inp] -> main bindir inp os world	
					
	= case errs of
		[]   -> case n of
					[]    -> error ["Missing input file name\n"] world
					[inp] -> main bindir inp os world
					_     -> error ["Too many input file names provided\n"] world
	
		errs -> error errs world

	
	

	